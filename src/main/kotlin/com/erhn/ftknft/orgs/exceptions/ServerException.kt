package com.erhn.ftknft.orgs.exceptions

sealed class ServerException(message: String) : Throwable(message) {


    class InvalidRequestBodyException() : ServerException(INVALID_REQUEST_BODY)
    class InvalidAccessTokenException() : ServerException(INVALID_ACCESS_TOKEN)
    class InvalidRefreshTokenException() : ServerException(INVALID_REFRESH_TOKEN)
    class LoginLengthException() : ServerException(INVALID_LOGIN_LENGTH)
    class PasswordLengthException() : ServerException(INVALID_PASSWORD_LENGTH)
    class LoginExistException() : ServerException(LOGIN_ALREADY_EXIST)
    class AuthUserException() : ServerException(INVALID_LOGIN_OR_PASSWORD)
    class NotSpecifiedWeightIdException : ServerException(NOT_SPECIFIED_WEIGHT_ID)
    class NotSpecifiedNoteIdException : ServerException(NOT_SPECIFIED_NOTE_ID)
    class NotSpecifiedCategoryIdException : ServerException(NOT_SPECIFIED_CATEGORY_ID)
    class WrongWeightIdException : ServerException(WRONG_WEIGHT_ID)
    class WrongNoteIdException : ServerException(WRONG_NOTE_ID)
    class WrongCategoryIdException : ServerException(WRONG_CATEGORY_ID)
    class EmptyCategoryNameException : ServerException(CATEGORY_NAME_IS_EMPTY)
    class WrongCategoryTypeException : ServerException(WRONG_CATEGORY_TYPE)
    class WrongExerciseIdException:ServerException(WRONG_EXERCISE_ID)
    class NotSpecifiedLoginException: ServerException(NOT_SPECIFIED_LOGIN)


    companion object {
        const val INVALID_REQUEST_BODY = "Invalid request body."
        const val INVALID_ACCESS_TOKEN = "Invalid access token."
        const val INVALID_REFRESH_TOKEN = "Invalid refresh token."
        const val INVALID_LOGIN_LENGTH = "Login must be longer than five characters."
        const val INVALID_PASSWORD_LENGTH = "Password must be longer than five characters."
        const val LOGIN_ALREADY_EXIST = "The login already exists."
        const val INVALID_LOGIN_OR_PASSWORD = "Invalid login or password."
        const val NOT_SPECIFIED_WEIGHT_ID = "Not specified weightId."
        const val NOT_SPECIFIED_NOTE_ID = "Not specified noteId."
        const val NOT_SPECIFIED_CATEGORY_ID = "Not specified category id."
        const val NOT_SPECIFIED_LOGIN = "Not specified login."
        const val WRONG_WEIGHT_ID = "Wrong weight id."
        const val WRONG_NOTE_ID = "Wrong note id."
        const val WRONG_CATEGORY_ID = "Wrong category id."
        const val CATEGORY_NAME_IS_EMPTY = "Category name is empty"
        const val WRONG_CATEGORY_TYPE = "Wrong category type"
        const val WRONG_EXERCISE_ID = "Wrong exercise id."
    }


}