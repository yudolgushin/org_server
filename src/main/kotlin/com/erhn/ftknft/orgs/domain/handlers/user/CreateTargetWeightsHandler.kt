package com.erhn.ftknft.orgs.domain.handlers.user

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.TargetWeightsDao
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.domain.handlers.base.AuthBodyHandler
import com.erhn.ftknft.orgs.domain.models.request.user.CreateTargetWeightRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class CreateTargetWeightsHandler(private val targetWeightsDao: TargetWeightsDao, accessTokenDao: AccessTokenDao, gson: Gson) :
    AuthBodyHandler<CreateTargetWeightRequest>(CreateTargetWeightRequest::class.java, accessTokenDao, gson) {

    override fun onSuccessHandle(event: RoutingContext, accessToken: Token, requestBody: CreateTargetWeightRequest) {
        val userId = accessToken.userId
        attemp(event) {
            targetWeightsDao.insert(userId, requestBody.targetWeight)
            event.endResponse(BaseResponse.justSuccess())
        }
    }
}