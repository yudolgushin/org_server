package com.erhn.ftknft.orgs.domain.handlers.admin

import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.utils.config.Config
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class AdminGetWeightsHandler(private val weightDao: WeightDao, config: Config, gson: Gson) : AdminBaseHandler(config, gson) {

    override fun onAdminValidateSuccess(event: RoutingContext) {
        attemp(event){
            val weights = weightDao.getAll()
            event.endResponse(BaseResponse(weights))
        }
    }
}