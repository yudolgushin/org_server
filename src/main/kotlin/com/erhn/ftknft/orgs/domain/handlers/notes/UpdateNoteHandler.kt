package com.erhn.ftknft.orgs.domain.handlers.notes

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.notes.NoteDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.data.model.socket.EventType
import com.erhn.ftknft.orgs.data.model.socket.ObjectType
import com.erhn.ftknft.orgs.data.model.socket.SocketModel
import com.erhn.ftknft.orgs.domain.handlers.base.AuthBodyHandler
import com.erhn.ftknft.orgs.domain.models.request.notes.CreateNoteRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.notes.NoteResponse
import com.erhn.ftknft.orgs.domain.socket.SocketConnectionManager
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class UpdateNoteHandler(private val noteDao: NoteDao, private val sc: SocketConnectionManager,
                        accessTokenDao: AccessTokenDao, gson: Gson) :
    AuthBodyHandler<CreateNoteRequest>(CreateNoteRequest::class.java, accessTokenDao, gson) {


    override fun onSuccessHandle(event: RoutingContext, accessToken: Token, requestBody: CreateNoteRequest) {
        try {
            val note = noteDao.firstFindByTwoParams(Pair(DbConst.ID, event.request().getParam(MainVerticle.NOTE_ID)),
                Pair(DbConst.USER_ID, accessToken.userId))
            if (note == null) {
                event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.WrongNoteIdException()), 404)
            } else {
                noteDao.update(note.id, requestBody.title, requestBody.text ?: note.text)
                val updatedNote = noteDao.firstFindByParam(DbConst.ID, note.id)
                event.endResponse(BaseResponse(BaseResponse.Status.SUCCESS))
                updatedNote?.let { w ->
                    sc.send(accessToken,
                        SocketModel(
                            ObjectType.NOTE, EventType.UPDATE,
                            System.currentTimeMillis(), NoteResponse(note)))
                }
            }
        } catch (e: Throwable) {
            event.unknownErrorResponse(e, this)
        }
    }
}