package com.erhn.ftknft.orgs.domain.handlers.admin

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.domain.handlers.base.BaseHandler
import com.erhn.ftknft.orgs.utils.config.Config
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

abstract class AdminBaseHandler(private val config: Config, gson: Gson) : BaseHandler(gson) {

    override fun handle(event: RoutingContext) {
        val secretKey = event.request().getParam(MainVerticle.SECRET_KEY)
        if (secretKey != config.secretKey()) {
            event.invalidAccessTokenResponse()
        } else {
            onAdminValidateSuccess(event)
        }
    }

    abstract fun onAdminValidateSuccess(event: RoutingContext)
}