package com.erhn.ftknft.orgs.domain.handlers.base

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.model.Token
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class DefaultAuthHandler(accessTokenDao: AccessTokenDao, gson: Gson) : AuthHandler(accessTokenDao, gson) {

    override fun onSuccessAuth(event: RoutingContext, accessToken: Token) {}

}