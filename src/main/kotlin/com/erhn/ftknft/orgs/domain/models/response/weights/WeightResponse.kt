package com.erhn.ftknft.orgs.domain.models.response.weights

import com.erhn.ftknft.orgs.data.model.Weight
import com.google.gson.annotations.SerializedName

class WeightResponse(
        val id: Long,
        @SerializedName("weight_value")
        val weightValue: Double,
        @SerializedName("weight_date")
        val weightDate: Long
) {
    constructor(weight: Weight) : this(weight.id, weight.weightValue, weight.dateWeight)
}