package com.erhn.ftknft.orgs.domain.handlers.notes

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.notes.NoteDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.data.model.socket.EventType
import com.erhn.ftknft.orgs.data.model.socket.ObjectType
import com.erhn.ftknft.orgs.data.model.socket.SocketModel
import com.erhn.ftknft.orgs.domain.handlers.base.AuthHandler
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.notes.NoteResponse
import com.erhn.ftknft.orgs.domain.socket.SocketConnectionManager
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class DeleteNoteHandler(
    private val sc: SocketConnectionManager, private val notesDao: NoteDao,
    accessTokenDao: AccessTokenDao, gson: Gson
) : AuthHandler(accessTokenDao, gson) {


    override fun onSuccessAuth(event: RoutingContext, accessToken: Token) {
        try {
            val noteId = event.request().getParam(MainVerticle.NOTE_ID)
            removeNoteById(event, noteId, accessToken)
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }
    }

    private fun removeNoteById(event: RoutingContext, noteId: String?, token: Token) {
        if (noteId == null) {
            event.endResponse(
                BaseResponse(BaseResponse.Status.ERROR, ServerException.NotSpecifiedNoteIdException()),
                400
            )
        } else {
            val note = notesDao.firstFindByTwoParams(Pair(DbConst.ID, noteId), Pair(DbConst.USER_ID, token.userId))
            if (note == null) {
                event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.WrongNoteIdException()), 404)
            } else {
                notesDao.removeById(note.id)
                event.endResponse(BaseResponse(BaseResponse.Status.SUCCESS))
                sc.send(
                    token, SocketModel(
                        ObjectType.NOTE, EventType.DELETE,
                        System.currentTimeMillis(), NoteResponse(note)
                    )
                )
            }


        }
    }
}