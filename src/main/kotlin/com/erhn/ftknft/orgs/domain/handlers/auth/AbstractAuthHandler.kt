package com.erhn.ftknft.orgs.domain.handlers.auth

import com.erhn.ftknft.orgs.domain.handlers.base.BodyHandler
import com.erhn.ftknft.orgs.domain.models.request.auth.AuthRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

abstract class AbstractAuthHandler(gson: Gson) : BodyHandler<AuthRequest>(AuthRequest::class.java, gson) {


    override fun onSuccess(event: RoutingContext, requestBody: AuthRequest) {
        try {
            if (validateAuthRequest(requestBody, event)) return
            onAuthReqValid(requestBody, event)
        } catch (e: Throwable) {
            event.unknownErrorResponse(e, this)
        }

    }

    private fun validateAuthRequest(reqBody: AuthRequest, event: RoutingContext): Boolean {
        if (reqBody.login.length < 6) {
            event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.LoginLengthException()), 400)
            return true
        }
        if (reqBody.passwordHash.length < 6) {
            event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.PasswordLengthException()), 400)
            return true
        }
        return false
    }

    protected abstract fun onAuthReqValid(reqBody: AuthRequest, event: RoutingContext)
}