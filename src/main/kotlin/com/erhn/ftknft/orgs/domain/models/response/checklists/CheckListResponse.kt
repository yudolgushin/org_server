package com.erhn.ftknft.orgs.domain.models.response.checklists

import com.erhn.ftknft.orgs.data.model.CheckElement
import com.erhn.ftknft.orgs.data.model.Checklist
import com.google.gson.annotations.SerializedName

data class CheckListResponse(
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val title: String,
    @SerializedName("create_date")
    val createDate: Long,
    @SerializedName("update_date")
    val updateDate: Long,
    @SerializedName("elements")
    val elements: List<CheckElementResponse>){

    constructor(checklist:Checklist,list: List<CheckElement>) : this(
        checklist.id,
        checklist.title,
        checklist.createDate,
        checklist.updateDate,
        list.map { CheckElementResponse(it) }
    )
}