package com.erhn.ftknft.orgs.domain.handlers.notes

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.notes.NoteDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.domain.handlers.base.AuthHandler
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.notes.NoteResponse
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class GetUserNotesHandler(val notesDao: NoteDao, accessTokenDao: AccessTokenDao, gson: Gson) :
    AuthHandler(accessTokenDao, gson) {


    override fun onSuccessAuth(event: RoutingContext, accessToken: Token) {
        try {
            val listResponse = notesDao.findByParam(DbConst.USER_ID, accessToken.userId).map { NoteResponse(it) }
                .sortedByDescending { it.updateDate }
            event.endResponse(BaseResponse(listResponse))
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }
    }
}