package com.erhn.ftknft.orgs.domain.handlers.weights

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.domain.handlers.base.AuthHandler
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.weights.WeightResponse
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class GetWeightByIdHandler(private val weDao: WeightDao, accessTokenDao: AccessTokenDao, gson: Gson) :
    AuthHandler(accessTokenDao, gson) {


    override fun onSuccessAuth(event: RoutingContext, accessToken: Token) {
        try {
            val id = event.request().getParam(MainVerticle.WEIGHT_ID)
            val weight = weDao.firstFindByTwoParams(Pair(DbConst.USER_ID, accessToken.userId), Pair(DbConst.ID, id))
            if (weight == null) {
                event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.WrongWeightIdException()),
                    404)
            } else {
                event.endResponse(BaseResponse(WeightResponse(weight)))
            }


        } catch (e: Throwable) {
            event.unknownErrorResponse(e, this)
        }
    }
}