package com.erhn.ftknft.orgs.domain.models.response.auth

import com.google.gson.annotations.SerializedName

class AuthorizationResponse(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("refresh_token")
    val refreshToken: String
)