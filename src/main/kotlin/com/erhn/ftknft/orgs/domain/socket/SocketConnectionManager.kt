package com.erhn.ftknft.orgs.domain.socket

import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.data.model.socket.SocketConnection
import com.erhn.ftknft.orgs.data.model.socket.SocketModel
import com.erhn.ftknft.orgs.utils.logln
import com.google.gson.Gson
import io.vertx.core.http.ServerWebSocket

class SocketConnectionManager(private val gson: Gson) {
    private val connections: HashMap<String, SocketConnection> = hashMapOf()


    fun addNew(token: Token, ws: ServerWebSocket) {
        connections.put(token.token, SocketConnection(token.userId, ws))
        logln("Add user:${token.userId}, token:${token.token}")
    }


    fun remove(token: Token) {
        connections.remove(token.token)
    }


    fun send(token: Token, socketModel: SocketModel) {
        connections.filter { it.value.userId == token.userId }.map {
            val element = gson.toJson(socketModel)
            it.value.write(element)
            logln("Send userId: ${token.userId}, token: ${token.token}, msg: $element")
        }


    }


    fun close(token: Token, code: Short = 500, reason: String = "Unknown") {
        val ws = connections[token.token]
        ws?.serverWebSocket?.close(code, reason)

    }

}