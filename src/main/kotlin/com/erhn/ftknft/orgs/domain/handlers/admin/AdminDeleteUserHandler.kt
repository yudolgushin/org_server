package com.erhn.ftknft.orgs.domain.handlers.admin

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.auth.UserDao
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.utils.config.Config
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class AdminDeleteUserHandler(private val usersDao: UserDao, config: Config, gson: Gson) : AdminBaseHandler(config, gson) {

    override fun onAdminValidateSuccess(event: RoutingContext) {
        attemp(event) {
            val userID = event.request().getParam(MainVerticle.USER_ID)
            usersDao.removeById(userID.toLong())
            event.endResponse(BaseResponse.justSuccess())
        }
    }
}