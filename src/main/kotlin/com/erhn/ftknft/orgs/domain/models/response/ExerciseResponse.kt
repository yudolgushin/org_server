package com.erhn.ftknft.orgs.domain.models.response

import com.erhn.ftknft.orgs.data.model.Exercise
import com.google.gson.annotations.SerializedName

data class ExerciseResponse(
    @SerializedName("id")
    val id:Long,
    @SerializedName("name")
    val name:String,
    @SerializedName("description")
    val description:String?,
    @SerializedName("type")
    val type:Exercise.Type
)