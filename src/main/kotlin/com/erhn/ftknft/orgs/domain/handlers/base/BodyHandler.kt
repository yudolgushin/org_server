package com.erhn.ftknft.orgs.domain.handlers.base

import com.erhn.ftknft.orgs.utils.Verifiable
import com.erhn.ftknft.orgs.utils.fromJsonVerify
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

abstract class BodyHandler<T : Verifiable>(private val clazz: Class<T>, gson: Gson) : BaseHandler(gson) {

    fun parseJson(json: String): T {
        return gson.fromJsonVerify(json, clazz)
    }

    override fun handle(event: RoutingContext) {
        try {
            handleBody(event)
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }

    }

    fun handleBody(event: RoutingContext, onSuccessBlock: (element: T) -> Unit = {}) {
        event.request().bodyHandler { buffer ->
            try {
                val requestBody = parseJson(buffer.toString())
                onSuccess(event, requestBody)
                onSuccessBlock(requestBody)
            } catch (e: Throwable) {
                event.invalidRequestBodyResponse()
            }

        }
    }

    abstract fun onSuccess(event: RoutingContext, requestBody: T)

}