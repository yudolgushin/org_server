package com.erhn.ftknft.orgs.domain.models.request.finance

import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class InsertCategoryRequest(
    @SerializedName("category_name")
    val categoryName: String,
    @SerializedName("category_type")
    val categoryType: String
) : Verifiable {
    override fun verify() {
        categoryName!!
        categoryType!!

    }
}