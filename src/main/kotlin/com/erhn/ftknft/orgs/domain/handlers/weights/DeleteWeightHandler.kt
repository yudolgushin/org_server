package com.erhn.ftknft.orgs.domain.handlers.weights

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.data.model.socket.EventType
import com.erhn.ftknft.orgs.data.model.socket.ObjectType
import com.erhn.ftknft.orgs.data.model.socket.SocketModel
import com.erhn.ftknft.orgs.domain.handlers.base.AuthHandler
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.weights.WeightResponse
import com.erhn.ftknft.orgs.domain.socket.SocketConnectionManager
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class DeleteWeightHandler(private val sc: SocketConnectionManager, private val weDao: WeightDao,
                          accessTokenDao: AccessTokenDao, gson: Gson) : AuthHandler(accessTokenDao, gson) {


    override fun onSuccessAuth(event: RoutingContext, accessToken: Token) {
        try {
            val weightId = event.request().getParam(MainVerticle.WEIGHT_ID)
            removeWeightById(event, weightId, accessToken)
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }
    }

    private fun removeWeightById(event: RoutingContext, weightId: String?, token: Token) {
        if (weightId == null) {
            event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.NotSpecifiedWeightIdException()),
                400)
        } else {
            val weight = weDao.firstFindByTwoParams(Pair(DbConst.ID, weightId), Pair(DbConst.USER_ID, token.userId))
            if (weight == null) {
                event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.WrongWeightIdException()),
                    404)
            } else {
                weDao.removeById(weight.id)
                event.endResponse(BaseResponse(BaseResponse.Status.SUCCESS))
                sc.send(token,
                    SocketModel(
                        ObjectType.WEIGHT, EventType.DELETE,
                        System.currentTimeMillis(), WeightResponse(weight.id, weight.weightValue, weight.dateWeight)))
            }


        }


    }
}