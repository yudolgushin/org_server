package com.erhn.ftknft.orgs.domain.handlers.user

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.TargetWeightsDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.domain.handlers.base.AuthBodyHandler
import com.erhn.ftknft.orgs.domain.models.request.user.SyncUserRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class SyncUserHandler(
    private val targetWeightsDao: TargetWeightsDao,
    private val weightDao: WeightDao,
    accessTokenDao: AccessTokenDao,
    gson: Gson
) :
    AuthBodyHandler<SyncUserRequest>(SyncUserRequest::class.java, accessTokenDao, gson) {

    override fun onSuccessHandle(event: RoutingContext, accessToken: Token, requestBody: SyncUserRequest) {
        val userId = accessToken.userId
        attemp(event) {
            targetWeightsDao.insert(userId, requestBody.targetWeight)
            weightDao.removeByParam(DbConst.USER_ID, userId)
            weightDao.insertAll(userId, requestBody.allWeight.map { Pair(it.weightValue, it.weightDate ?: System.currentTimeMillis()) })
            event.endResponse(BaseResponse.justSuccess())
        }
    }
}