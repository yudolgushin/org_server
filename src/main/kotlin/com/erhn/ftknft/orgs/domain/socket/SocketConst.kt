package com.erhn.ftknft.orgs.domain.socket

object SocketConst {

    const val WEIGHTS = "WEIGHTS"

    const val NOTES = "NOTES"

    const val EXERCISES = "EXERCISES"

    const val UPDATE = "UPDATED"

    const val REMOVE = "REMOVED"

    const val INSERTED = "INSERTED"

}