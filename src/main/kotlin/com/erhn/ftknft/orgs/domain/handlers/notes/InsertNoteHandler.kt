package com.erhn.ftknft.orgs.domain.handlers.notes

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.notes.NoteDao
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.data.model.socket.EventType
import com.erhn.ftknft.orgs.data.model.socket.ObjectType
import com.erhn.ftknft.orgs.data.model.socket.SocketModel
import com.erhn.ftknft.orgs.domain.handlers.base.AuthBodyHandler
import com.erhn.ftknft.orgs.domain.models.request.notes.CreateNoteRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.notes.NoteResponse
import com.erhn.ftknft.orgs.domain.socket.SocketConnectionManager
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class InsertNoteHandler(accessTokenDao: AccessTokenDao, gson: Gson, private val noteDao: NoteDao,
                        private val sc: SocketConnectionManager) :
    AuthBodyHandler<CreateNoteRequest>(CreateNoteRequest::class.java, accessTokenDao, gson) {

    override fun onSuccessHandle(event: RoutingContext, accessToken: Token, requestBody: CreateNoteRequest) {
        try {
            noteDao.insert(accessToken.userId, requestBody.title, requestBody.text ?: "")
            val insertedNote = noteDao.lastInserted()
            event.endResponse(BaseResponse(BaseResponse.Status.SUCCESS))
            insertedNote?.let {
                sc.send(accessToken,
                    SocketModel(ObjectType.NOTE, EventType.ADD, System.currentTimeMillis(), NoteResponse(it)))
            }
        } catch (e: Throwable) {
            event.unknownErrorResponse(e, this)
        }

    }
}
