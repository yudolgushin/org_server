package com.erhn.ftknft.orgs.domain.socket

import com.erhn.ftknft.orgs.data.db.Db
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.utils.logln
import io.vertx.core.Handler
import io.vertx.core.http.ServerWebSocket

class WebSocketHandler(private val sm: SocketConnectionManager, private val db: Db) : Handler<ServerWebSocket> {


    override fun handle(event: ServerWebSocket) {
        val token = event.headers()["Authorization"]
        if (token.isNullOrEmpty()) {
            event.reject(401)
        } else {
            val tokenModel = db.accessTokenDao.firstFindByParam(DbConst.TOKEN, token)
            if (tokenModel != null) {
                if (tokenModel.expirationTime > System.currentTimeMillis()) {
                    sm.addNew(tokenModel, event)
                    event.closeHandler {
                        logln("Close socket by userId: ${tokenModel.userId}, token: ${tokenModel.token}")
                        sm.remove(tokenModel)
                    }
                    event.exceptionHandler {
                        logln("Exception socket by ${tokenModel.token}, ${it.localizedMessage}")
                        sm.remove(tokenModel)
                    }
                } else {
                    event.reject(401)
                }

            } else {
                event.reject(401)
            }

        }
    }
}