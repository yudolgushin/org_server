package com.erhn.ftknft.orgs.domain.handlers.weights

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.data.model.socket.EventType
import com.erhn.ftknft.orgs.data.model.socket.ObjectType
import com.erhn.ftknft.orgs.data.model.socket.SocketModel
import com.erhn.ftknft.orgs.domain.handlers.base.AuthBodyHandler
import com.erhn.ftknft.orgs.domain.models.request.weights.UpdateWeightRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.weights.WeightResponse
import com.erhn.ftknft.orgs.domain.socket.SocketConnectionManager
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class UpdateWeightHandler(private val sc: SocketConnectionManager, private val weDao: WeightDao,
                          accessTokenDao: AccessTokenDao, gson: Gson) :
    AuthBodyHandler<UpdateWeightRequest>(UpdateWeightRequest::class.java, accessTokenDao, gson) {

    override fun onSuccessHandle(event: RoutingContext, accessToken: Token, requestBody: UpdateWeightRequest) {
        try {
            val weight = weDao.firstFindByTwoParams(Pair(DbConst.ID, event.request().getParam(MainVerticle.WEIGHT_ID)),
                Pair(DbConst.USER_ID, accessToken.userId))
            if (weight == null) {
                event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.WrongWeightIdException()),
                    404)
            } else {
                weDao.update(requestBody.weightValue ?: weight.weightValue, requestBody.weightDate ?: weight.dateWeight,
                    weight.id)
                val updatedWeight = weDao.firstFindByParam(DbConst.ID, weight.id)
                event.endResponse(BaseResponse(BaseResponse.Status.SUCCESS))
                updatedWeight?.let { w ->
                    sc.send(accessToken,
                        SocketModel(
                            ObjectType.WEIGHT, EventType.UPDATE,
                            System.currentTimeMillis(), WeightResponse(w.id, w.weightValue, w.dateWeight)))
                }
            }
        } catch (e: Throwable) {
            event.unknownErrorResponse(e, this)
        }
    }
}