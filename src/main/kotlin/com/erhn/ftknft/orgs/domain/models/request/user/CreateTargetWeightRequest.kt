package com.erhn.ftknft.orgs.domain.models.request.user

import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class CreateTargetWeightRequest(
    @SerializedName("value")
    val targetWeight: Double?
) : Verifiable {
    override fun verify() {
        targetWeight
    }
}