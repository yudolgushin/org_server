package com.erhn.ftknft.orgs.domain.models.request.notes

import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class CreateNoteRequest(
    @SerializedName("title")
    val title:String,
    @SerializedName("text")
    val text:String?
):Verifiable{
    override fun verify() {
        title!!
    }
}
