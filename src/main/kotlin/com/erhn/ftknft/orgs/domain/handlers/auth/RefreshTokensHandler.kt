package com.erhn.ftknft.orgs.domain.handlers.auth

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.auth.RefreshTokenDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.domain.handlers.base.EmptyHandler
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.auth.AuthorizationResponse
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.erhn.ftknft.orgs.utils.TokenUtil
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class RefreshTokensHandler(private val atDao: AccessTokenDao, private val rfDao: RefreshTokenDao, gson: Gson) :
    EmptyHandler(gson) {

    override fun handle(event: RoutingContext) {
        try {
            val header = event.request().getHeader(MainVerticle.AUTHORIZATION_HEADER)
            if (header == null) {
                event.endResponse(
                    BaseResponse(BaseResponse.Status.ERROR, ServerException.InvalidRefreshTokenException()), 401)
            } else {
                val token = rfDao.firstFindByParam(DbConst.TOKEN, header)
                if (token == null || token.expirationTime < System.currentTimeMillis()) {
                    event.endResponse(
                        BaseResponse(BaseResponse.Status.ERROR, ServerException.InvalidRefreshTokenException()), 401)
                    return
                } else {
                    val accessToken = TokenUtil.generate()
                    val refreshToken = TokenUtil.generate()
                    atDao.insert(accessToken, token.userId)
                    rfDao.insert(refreshToken, token.userId)
                    event.endResponse(BaseResponse(AuthorizationResponse(accessToken, refreshToken)))
                }
            }
        } catch (e: Throwable) {
            event.unknownErrorResponse(e, this)
        }
    }

}