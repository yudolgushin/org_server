package com.erhn.ftknft.orgs.domain.handlers.weights

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.domain.handlers.base.AuthHandler
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.weights.WeightResponse
import com.erhn.ftknft.orgs.utils.log
import com.erhn.ftknft.orgs.utils.toDateFormat
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext
import java.util.*


class GetUserWeightsMonthHandler(private val weDao: WeightDao, accessTokenDao: AccessTokenDao, gson: Gson) :
    AuthHandler(accessTokenDao, gson) {


    override fun onSuccessAuth(event: RoutingContext, accessToken: Token) {
        try {
            val startDate = getFirstDateOfMonth().time

            val endDate = getLastDateOfMonth().time
            log("startDate = ${startDate.toDateFormat()}, endDate =${endDate.toDateFormat()}")
            val list = weDao.findWithFilters(accessToken.userId, startDate, endDate).map { WeightResponse(it) }
            event.endResponse(BaseResponse(list))
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }
    }

    fun getFirstDateOfMonth(): Date {
        val date = Date()
        val calendar = Calendar.getInstance()
        calendar.apply {
            time = date
            set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH))
            set(Calendar.HOUR_OF_DAY, calendar.getActualMinimum(Calendar.HOUR_OF_DAY))
            set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE))
            set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND))
        }
        return calendar.time
    }

    fun getLastDateOfMonth(): Date {
        val date = Date()
        val calendar = Calendar.getInstance()
        calendar.apply {
            time = date
            set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
            set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY))
            set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE))
            set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND))
        }
        return calendar.time
    }

    companion object {
        const val START_DATE = "startDate"
        const val END_DATE = "endDate"
    }
}