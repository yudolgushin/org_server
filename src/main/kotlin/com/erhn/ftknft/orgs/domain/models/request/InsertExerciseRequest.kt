package com.erhn.ftknft.orgs.domain.models.request

import com.erhn.ftknft.orgs.data.model.Exercise
import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class InsertExerciseRequest(
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: Exercise.Type,
    @SerializedName("description")
    val description:String
):Verifiable{

    override fun verify() {
        name!!
        type!!
    }

}