package com.erhn.ftknft.orgs.domain.handlers.admin

import com.erhn.ftknft.orgs.data.dao.imp.notes.NoteDao
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.utils.config.Config
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class AdminGetNotesHandler(private val notesDao: NoteDao, config: Config, gson: Gson) : AdminBaseHandler(config, gson) {

    override fun onAdminValidateSuccess(event: RoutingContext) {
        attemp(event){
            val weights = notesDao.getAll()
            event.endResponse(BaseResponse(weights))
        }
    }
}