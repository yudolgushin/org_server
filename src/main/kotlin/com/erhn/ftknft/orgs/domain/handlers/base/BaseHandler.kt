package com.erhn.ftknft.orgs.domain.handlers.base

import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.erhn.ftknft.orgs.utils.log
import com.google.gson.Gson
import io.netty.handler.codec.http.HttpHeaderNames
import io.netty.handler.codec.http.HttpHeaderValues
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.koin.core.KoinComponent

abstract class BaseHandler(protected val gson: Gson) : Handler<RoutingContext>, KoinComponent {


    protected fun RoutingContext.endResponse(responseBody: BaseResponse, code: Int = 200) {
        val response = request().response()
        if (response.ended()) return
        val respObj = gson.toJson(responseBody)
        response.putHeader(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
            .putHeader("Server-version", "0.4.0").putHeader("Server-time", System.currentTimeMillis().toString())
            .setStatusCode(code)
        log("${request().path()}> $respObj")
        println()
        response.end(respObj)

    }

    protected fun RoutingContext.invalidRequestBodyResponse() {
        endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.InvalidRequestBodyException()), 400)
    }

    protected fun RoutingContext.invalidAccessTokenResponse() {
        endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.InvalidAccessTokenException()), 401)
    }

    protected fun RoutingContext.unknownErrorResponse(e: Throwable, target: Any) {
        endResponse(
            BaseResponse(BaseResponse.Status.UNKNOWN_ERROR, "${target.javaClass.simpleName} - ${e.localizedMessage}"),
            500)
    }

    protected fun attemp(event: RoutingContext, block: () -> Unit) {
        try {
            block()
        } catch (e: Throwable) {
            event.unknownErrorResponse(e, this)
        }
    }

}