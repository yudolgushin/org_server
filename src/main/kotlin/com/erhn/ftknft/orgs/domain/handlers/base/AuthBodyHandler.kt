package com.erhn.ftknft.orgs.domain.handlers.base

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

abstract class AuthBodyHandler<T : Verifiable>(clazz: Class<T>, private val accessTokenDao: AccessTokenDao,
                                               gson: Gson) : BodyHandler<T>(clazz, gson) {


    private val authHandler = DefaultAuthHandler(accessTokenDao, gson)


    override fun handle(event: RoutingContext) {
        authHandler.handleAuthHeader(event)?.let { token ->
            handleBody(event) {
                onSuccessHandle(event, token, it)
            }
        }
    }

    override fun onSuccess(event: RoutingContext, requestBody: T) {}

    abstract fun onSuccessHandle(event: RoutingContext, accessToken: Token, requestBody: T)

}