package com.erhn.ftknft.orgs.domain.handlers.weights

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.domain.handlers.base.AuthHandler
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.weights.WeightResponse
import com.erhn.ftknft.orgs.utils.log
import com.erhn.ftknft.orgs.utils.toDateFormat
import com.erhn.ftknft.orgs.utils.toLongOrCurrentDateMs
import com.erhn.ftknft.orgs.utils.toLongOrZero
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class GetUserWeightsHandler(private val weDao: WeightDao, acceessTokenDao: AccessTokenDao, gson: Gson) :
    AuthHandler(acceessTokenDao, gson) {


    override fun onSuccessAuth(event: RoutingContext, accessToken: Token) {
        try {
            val startDate = event.queryParam(START_DATE).firstOrNull()?.toLongOrZero() ?: 0
            val endDate = event.queryParam(END_DATE).firstOrNull()?.toLongOrCurrentDateMs() ?: Long.MAX_VALUE
            val limit = event.queryParam(LIMIT).firstOrNull()?.toLongOrNull()
            val page = event.queryParam(PAGE).firstOrNull()?.toLongOrNull()
            log("startDate = ${startDate.toDateFormat()}, endDate = ${endDate.toDateFormat()}")
            val list = weDao.findWithFilters(accessToken.userId, startDate, endDate, page, limit).map { WeightResponse(it) }
            event.endResponse(BaseResponse(list))
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }
    }

    companion object {
        const val START_DATE = "startDate"
        const val END_DATE = "endDate"
        const val LIMIT = "limit"
        const val PAGE = "page"
    }
}