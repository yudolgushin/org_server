package com.erhn.ftknft.orgs.domain.models.request.weights

import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class InsertWeightRequest(
        @SerializedName("weight_value")
        val weightValue: Double,
        @SerializedName("weight_date")
        val weightDate: Long?) : Verifiable {

    override fun verify() {
        weightValue!!
        if (weightDate==0L || weightValue==0.0) error(this)
    }
}