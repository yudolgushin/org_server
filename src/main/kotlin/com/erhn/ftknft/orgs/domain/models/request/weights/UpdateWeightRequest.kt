package com.erhn.ftknft.orgs.domain.models.request.weights

import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class UpdateWeightRequest(
        @SerializedName("weight_value")
        val weightValue: Double?,
        @SerializedName("weight_date")
        val weightDate: Long?
):Verifiable{
        override fun verify() {
        }
}