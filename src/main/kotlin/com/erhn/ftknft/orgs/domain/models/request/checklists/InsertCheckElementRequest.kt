package com.erhn.ftknft.orgs.domain.models.request.checklists

import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class InsertCheckElementRequest(@SerializedName("name") val name: String, @SerializedName("is_complete")
val isComplete: Boolean) : Verifiable {

    override fun verify() {
        name!!
        isComplete!!
    }

}
