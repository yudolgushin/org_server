package com.erhn.ftknft.orgs.domain.handlers.auth

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.auth.RefreshTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.auth.UserDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.domain.models.request.auth.AuthRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.auth.AuthorizationResponse
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.erhn.ftknft.orgs.utils.TokenUtil
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class SignInHandler(val atDao: AccessTokenDao, val rfDao: RefreshTokenDao, val uDao: UserDao, gson: Gson) :
    AbstractAuthHandler(gson) {

    override fun onAuthReqValid(reqBody: AuthRequest, event: RoutingContext) {
        try {
            val user = uDao.firstFindByTwoParams(Pair(DbConst.LOGIN, reqBody.login),
                Pair(DbConst.PASSWORD_HASH, reqBody.passwordHash))
            if (user == null) {
                event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.AuthUserException()), 400)
            } else {
                val accessToken = TokenUtil.generate()
                val refreshToken = TokenUtil.generate()
                atDao.insert(accessToken, user.id)
                rfDao.insert(refreshToken, user.id)
                event.endResponse(BaseResponse(AuthorizationResponse(accessToken, refreshToken)))
            }
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }

    }
}