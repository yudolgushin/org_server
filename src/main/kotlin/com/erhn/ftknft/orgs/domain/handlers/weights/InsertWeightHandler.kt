package com.erhn.ftknft.orgs.domain.handlers.weights

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.data.model.socket.EventType
import com.erhn.ftknft.orgs.data.model.socket.ObjectType
import com.erhn.ftknft.orgs.data.model.socket.SocketModel
import com.erhn.ftknft.orgs.domain.handlers.base.AuthBodyHandler
import com.erhn.ftknft.orgs.domain.models.request.weights.InsertWeightRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.weights.WeightResponse
import com.erhn.ftknft.orgs.domain.socket.SocketConnectionManager
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class InsertWeightHandler(
    private val sc: SocketConnectionManager, private val weDao: WeightDao, gson: Gson,
    accessTokenDao: AccessTokenDao
) :
    AuthBodyHandler<InsertWeightRequest>(InsertWeightRequest::class.java, accessTokenDao, gson) {


    override fun onSuccessHandle(event: RoutingContext, accessToken: Token, requestBody: InsertWeightRequest) {
        try {
            weDao.insertNew(
                accessToken.userId, requestBody.weightValue,
                requestBody.weightDate ?: System.currentTimeMillis()
            )
            val insertedWeight = weDao.lastInserted()
            event.endResponse(BaseResponse(WeightResponse(insertedWeight!!)))
            insertedWeight.let {
                sc.send(
                    accessToken,
                    SocketModel(
                        ObjectType.WEIGHT, EventType.ADD,
                        System.currentTimeMillis(), WeightResponse(it.id, it.weightValue, it.dateWeight)
                    )
                )
            }
        } catch (e: Throwable) {
            event.unknownErrorResponse(e, this)
        }
    }

}