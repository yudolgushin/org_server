package com.erhn.ftknft.orgs.domain.models.response.checklists

import com.erhn.ftknft.orgs.data.model.CheckElement
import com.google.gson.annotations.SerializedName

data class CheckElementResponse(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("is_complete")
    val isComplete: Boolean
){
    constructor(element:CheckElement) : this(element.id, element.name, element.isComplete)
}