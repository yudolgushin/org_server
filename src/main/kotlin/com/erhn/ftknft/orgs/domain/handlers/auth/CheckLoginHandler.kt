package com.erhn.ftknft.orgs.domain.handlers.auth

import com.erhn.ftknft.orgs.data.dao.imp.auth.UserDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.domain.handlers.base.EmptyHandler
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class CheckLoginHandler(private val usersDao: UserDao, gson: Gson) : EmptyHandler(gson) {

    override fun handle(event: RoutingContext) {
        attemp(event) {
            val login = event.request().getHeader(CHECK_LOGIN_HEADER)
            if (login != null) {
                if (login.length < 6) {
                    event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.LoginLengthException()), 400)
                    return@attemp
                }
                usersDao.firstFindByParam(DbConst.LOGIN, login)?.let {
                    event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.LoginExistException()), 400)
                    return@attemp
                }
                event.endResponse(BaseResponse.justSuccess())
            } else {
                event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.NotSpecifiedLoginException()), 400)
            }
        }
    }


    companion object {
        const val CHECK_LOGIN_HEADER = "Check-login"
    }
}