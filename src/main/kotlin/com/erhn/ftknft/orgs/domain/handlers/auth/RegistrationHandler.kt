package com.erhn.ftknft.orgs.domain.handlers.auth

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.auth.RefreshTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.auth.UserDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.domain.models.request.auth.AuthRequest
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.domain.models.response.auth.AuthorizationResponse
import com.erhn.ftknft.orgs.exceptions.ServerException
import com.erhn.ftknft.orgs.utils.TokenUtil
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class RegistrationHandler(private val atDao: AccessTokenDao, private val rfDao: RefreshTokenDao,
                          private val uDao: UserDao, gson: Gson) : AbstractAuthHandler(gson) {

    override fun onAuthReqValid(reqBody: AuthRequest, event: RoutingContext) {
        try {
            if (!uDao.findByParam(DbConst.LOGIN, reqBody.login).isEmpty()) {
                event.endResponse(BaseResponse(BaseResponse.Status.ERROR, ServerException.LoginExistException()), 400)
                return
            }
            val userId = uDao.insert(reqBody.login, reqBody.passwordHash, reqBody.displayName)
            val accessToken = TokenUtil.generate()
            val refreshToken = TokenUtil.generate()
            atDao.insert(accessToken, userId)
            rfDao.insert(refreshToken, userId)
            event.endResponse(BaseResponse(AuthorizationResponse(accessToken, refreshToken)))
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }

    }
}