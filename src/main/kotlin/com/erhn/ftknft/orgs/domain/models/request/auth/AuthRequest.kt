package com.erhn.ftknft.orgs.domain.models.request.auth

import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

class AuthRequest(
    val login: String,
    @SerializedName("password")
    val passwordHash: String,
    @SerializedName("display_name")
    val displayName: String?
) : Verifiable {
    override fun verify() {
        login!!
        passwordHash!!
    }
}