package com.erhn.ftknft.orgs.domain.models.request.checklists

import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class InsertChecklistRequest(@SerializedName("title") val title: String, @SerializedName("elements")
val elements: List<InsertCheckElementRequest>) : Verifiable {

    override fun verify() {
        title!!
        elements!!
    }

}