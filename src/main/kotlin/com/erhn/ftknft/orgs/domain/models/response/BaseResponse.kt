package com.erhn.ftknft.orgs.domain.models.response

import com.erhn.ftknft.orgs.exceptions.ServerException
import com.google.gson.annotations.SerializedName

class BaseResponse(
        val status: String,
        val data: Any?,
        @SerializedName("error_message")
        val errorMessage: String?
) {

    constructor(data: Any) : this(Status.SUCCESS.value, data, null) {}

    constructor(status: Status, errorMessage: String) : this(status.value, null, errorMessage)

    constructor(status: Status, t: ServerException) : this(status.value, null, t.message)

    constructor(status: Status) : this(status.value, null, null)


    enum class Status(val value: String) {
        SUCCESS("success"), ERROR("error"), UNKNOWN_ERROR("unknown_error")
    }

    companion object{
        fun justSuccess() = BaseResponse(Status.SUCCESS)
    }
}