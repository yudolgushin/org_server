package com.erhn.ftknft.orgs.domain.models.request.user

import com.erhn.ftknft.orgs.domain.models.request.weights.InsertWeightRequest
import com.erhn.ftknft.orgs.utils.Verifiable
import com.google.gson.annotations.SerializedName

data class SyncUserRequest(
    @SerializedName("target_weight_value")
    val targetWeight: Double?,
    @SerializedName("weights")
    val allWeight: List<InsertWeightRequest>
) : Verifiable {
    override fun verify() {}
}