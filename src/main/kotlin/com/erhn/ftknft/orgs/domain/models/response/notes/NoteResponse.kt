package com.erhn.ftknft.orgs.domain.models.response.notes

import com.erhn.ftknft.orgs.data.model.Note
import com.google.gson.annotations.SerializedName

class NoteResponse(
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val title: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("create_date")
    val createDate: Long,
    @SerializedName("update_date")
    val updateDate: Long
) {
    constructor(note: Note) : this(note.id, note.title, note.text, note.createDate, note.updateDate)
}