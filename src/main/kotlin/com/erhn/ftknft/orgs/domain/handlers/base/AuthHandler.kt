package com.erhn.ftknft.orgs.domain.handlers.base

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.model.Token
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

abstract class AuthHandler(private val accessTokenDao: AccessTokenDao, gson: Gson) : BaseHandler(gson) {


    override fun handle(event: RoutingContext) {
        try {
            handleAuthHeader(event)
        } catch (t: Throwable) {
            event.unknownErrorResponse(t, this)
        }
    }

    fun handleAuthHeader(event: RoutingContext): Token? {
        val token = checkAccessToken(event)
        if (token != null) {
            onSuccessAuth(event, token)
        } else {
            event.invalidAccessTokenResponse()
        }
        return token
    }


    fun checkAccessToken(event: RoutingContext): Token? {
        val token = event.request().getHeader(MainVerticle.AUTHORIZATION_HEADER)
        token?.let { t ->
            val p = accessTokenDao.getToken(token)
            if (p?.expirationTime ?: 0 < System.currentTimeMillis()) {
                return null
            } else {
                return p
            }

        }
        return null
    }


    abstract fun onSuccessAuth(event: RoutingContext, accessToken: Token)


}