package com.erhn.ftknft.orgs.domain.handlers.admin

import com.erhn.ftknft.orgs.MainVerticle
import com.erhn.ftknft.orgs.data.dao.imp.notes.NoteDao
import com.erhn.ftknft.orgs.domain.models.response.BaseResponse
import com.erhn.ftknft.orgs.utils.config.Config
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class AdminDeleteNoteHandler(private val notesDao: NoteDao, config: Config, gson: Gson) : AdminBaseHandler(config, gson) {

    override fun onAdminValidateSuccess(event: RoutingContext) {
        attemp(event) {
            val noteId = event.request().getParam(MainVerticle.NOTE_ID)
            notesDao.removeById(noteId.toLong())
            event.endResponse(BaseResponse.justSuccess())
        }
    }
}