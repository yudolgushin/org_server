package com.erhn.ftknft.orgs

import com.erhn.ftknft.orgs.data.db.Db
import com.erhn.ftknft.orgs.domain.handlers.admin.*
import com.erhn.ftknft.orgs.domain.handlers.auth.CheckLoginHandler
import com.erhn.ftknft.orgs.domain.handlers.auth.RefreshTokensHandler
import com.erhn.ftknft.orgs.domain.handlers.auth.RegistrationHandler
import com.erhn.ftknft.orgs.domain.handlers.auth.SignInHandler
import com.erhn.ftknft.orgs.domain.handlers.notes.DeleteNoteHandler
import com.erhn.ftknft.orgs.domain.handlers.notes.GetUserNotesHandler
import com.erhn.ftknft.orgs.domain.handlers.notes.InsertNoteHandler
import com.erhn.ftknft.orgs.domain.handlers.notes.UpdateNoteHandler
import com.erhn.ftknft.orgs.domain.handlers.user.CreateTargetWeightsHandler
import com.erhn.ftknft.orgs.domain.handlers.user.SyncUserHandler
import com.erhn.ftknft.orgs.domain.handlers.weights.*
import com.erhn.ftknft.orgs.domain.socket.WebSocketHandler
import com.erhn.ftknft.orgs.server.Path
import com.erhn.ftknft.orgs.utils.config.Config
import io.vertx.core.AbstractVerticle
import io.vertx.ext.web.Router
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.concurrent.TimeUnit

class MainVerticle : AbstractVerticle(), KoinComponent {

    val config by inject<Config>()

    override fun start() {
        super.start()
        val db = getKoin().get<Db>()
        val router = Router.router(vertx)
        //auth
        router.post("/${Path.REGISTRATION}").handler(getKoin().get<RegistrationHandler>())
        router.post("/${Path.AUTH}").handler(getKoin().get<SignInHandler>())
        router.get("/${Path.REFRESH}").handler(getKoin().get<RefreshTokensHandler>())
        router.get("/${Path.CHECK_LOGIN}").handler(getKoin().get<CheckLoginHandler>())
        //USER
        router.post("/${Path.USER}/${Path.SYNC}").handler(getKoin().get<SyncUserHandler>())
        router.post("/${Path.USER}/${Path.TARGET}").handler(getKoin().get<CreateTargetWeightsHandler>())
        //weights
        router.post("/${Path.WEIGHTS}").handler(getKoin().get<InsertWeightHandler>())
        router.get("/${Path.WEIGHTS}").handler(getKoin().get<GetUserWeightsHandler>())
        router.get("/${Path.WEIGHTS}/$MONTH").handler(getKoin().get<GetUserWeightsMonthHandler>())
        router.delete("/${Path.WEIGHTS}/:$WEIGHT_ID").handler(getKoin().get<DeleteWeightHandler>())
        router.get("/${Path.WEIGHTS}/:$WEIGHT_ID").handler(getKoin().get<GetWeightByIdHandler>())
        router.put("/${Path.WEIGHTS}/:$WEIGHT_ID").handler(getKoin().get<UpdateWeightHandler>())
        //Notes
        router.post("/${Path.NOTES}").handler(getKoin().get<InsertNoteHandler>())
        router.get("/${Path.NOTES}").handler(getKoin().get<GetUserNotesHandler>())
        router.delete("/${Path.NOTES}/:$NOTE_ID").handler(getKoin().get<DeleteNoteHandler>())
        router.put("/${Path.NOTES}/:$NOTE_ID").handler(getKoin().get<UpdateNoteHandler>())
        //Admin
        router.get("/${Path.ADMIN}/:$SECRET_KEY/${Path.USERS}").handler(getKoin().get<AdminGetUsersHandler>())
        router.get("/${Path.ADMIN}/:$SECRET_KEY/${Path.WEIGHTS}").handler(getKoin().get<AdminGetWeightsHandler>())
        router.get("/${Path.ADMIN}/:$SECRET_KEY/${Path.NOTES}").handler(getKoin().get<AdminGetNotesHandler>())
        router.delete("/${Path.ADMIN}/:$SECRET_KEY/${Path.USERS}/:$USER_ID").handler(getKoin().get<AdminDeleteUserHandler>())
        router.delete("/${Path.ADMIN}/:$SECRET_KEY/${Path.WEIGHTS}/:$WEIGHT_ID").handler(getKoin().get<AdminDeleteWeightHandler>())
        router.delete("/${Path.ADMIN}/:$SECRET_KEY/${Path.NOTES}/:$NOTE_ID").handler(getKoin().get<AdminDeleteNoteHandler>())
        startServer(router)

        vertx.setPeriodic(TimeUnit.DAYS.toMillis(1), {
            db.removeExpiredTokens()
        })


    }

    private fun startServer(router: Router) {
        println("Starting server at port $PORT...")
        val wsHandler = getKoin().get<WebSocketHandler>()
        vertx.createHttpServer().requestHandler(router::handle).websocketHandler(wsHandler).listen(PORT)
        println("Server started")

    }


    companion object {
        const val AUTHORIZATION_HEADER = "Authorization"
        const val PORT = 8181

        const val MONTH = "month"
        const val WEIGHT_ID = "weightId"
        const val NOTE_ID = "noteId"
        const val SECRET_KEY = "secretKey"
        const val USER_ID = "userId"

    }
}