package com.erhn.ftknft.orgs

import com.erhn.ftknft.orgs.data.db.Db
import com.erhn.ftknft.orgs.domain.handlers.admin.*
import com.erhn.ftknft.orgs.domain.handlers.auth.CheckLoginHandler
import com.erhn.ftknft.orgs.domain.handlers.auth.RefreshTokensHandler
import com.erhn.ftknft.orgs.domain.handlers.auth.RegistrationHandler
import com.erhn.ftknft.orgs.domain.handlers.auth.SignInHandler
import com.erhn.ftknft.orgs.domain.handlers.notes.DeleteNoteHandler
import com.erhn.ftknft.orgs.domain.handlers.notes.GetUserNotesHandler
import com.erhn.ftknft.orgs.domain.handlers.notes.InsertNoteHandler
import com.erhn.ftknft.orgs.domain.handlers.notes.UpdateNoteHandler
import com.erhn.ftknft.orgs.domain.handlers.user.CreateTargetWeightsHandler
import com.erhn.ftknft.orgs.domain.handlers.user.SyncUserHandler
import com.erhn.ftknft.orgs.domain.handlers.weights.*
import com.erhn.ftknft.orgs.domain.socket.SocketConnectionManager
import com.erhn.ftknft.orgs.domain.socket.WebSocketHandler
import com.erhn.ftknft.orgs.utils.config.Config
import com.google.gson.Gson
import org.koin.dsl.module

private val db = Db()

val utilsModule = module {
    single { Config(get()) }
    factory { Gson() }
}

val dataModule = module {
    single { db }
    single { SocketConnectionManager(get()) }
    factory { db.accessTokenDao }
    factory { db.refreshTokenDao }
    factory { db.userDao }

    factory { db.weightDao }
    factory { db.noteDao }
    factory { db.targetWeightDao }
}

val handlersModule = module {
    factory { SignInHandler(get(), get(), get(), get()) }
    factory { RegistrationHandler(get(), get(), get(), get()) }
    factory { RefreshTokensHandler(get(), get(), get()) }
    //WEIGHTS
    factory { DeleteWeightHandler(get(), get(), get(), get()) }
    factory { GetUserWeightsHandler(get(), get(), get()) }
    factory { GetWeightByIdHandler(get(), get(), get()) }
    factory { GetUserWeightsMonthHandler(get(), get(), get()) }
    factory { InsertWeightHandler(get(), get(), get(), get()) }
    factory { UpdateWeightHandler(get(), get(), get(), get()) }
    //TARGET
    factory { CreateTargetWeightsHandler(get(), get(), get()) }
    factory { SyncUserHandler(get(), get(), get(), get()) }

    //NOTES
    factory { InsertNoteHandler(get(), get(), get(), get()) }
    factory { GetUserNotesHandler(get(), get(), get()) }
    factory { DeleteNoteHandler(get(), get(), get(), get()) }
    factory { UpdateNoteHandler(get(), get(), get(), get()) }
    //ADMIN
    factory { AdminGetUsersHandler(get(), get(), get()) }
    factory { AdminGetWeightsHandler(get(), get(), get()) }
    factory { AdminGetNotesHandler(get(), get(), get()) }
    factory { AdminDeleteUserHandler(get(), get(), get()) }
    factory { AdminDeleteWeightHandler(get(), get(), get()) }
    factory { AdminDeleteNoteHandler(get(), get(), get()) }
    factory { CheckLoginHandler(get(), get()) }

    factory { WebSocketHandler(get(), get()) }
}