package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.*
import com.erhn.ftknft.querybuilder.core.columnconstraint.InitColumnsQueryChain
import com.erhn.ftknft.querybuilder.imp.columnconstraint.InitColumnsQueryChainImp

class InitialQueryChainImp(query: String) : BaseQueryChain(query),
    InitialQueryChain {
    override fun select(vararg columns: String): FromQueryChain {
        query += SELECT
        if (columns.size == 0) {
            query += " $STAR"
        } else {
            columns.forEachIndexed { index, s ->
                if (index == 0) {
                    query += " $s"
                } else {
                    query += ", $s"
                }
            }
        }
        return FromQueryChainImp(query)
    }

    override fun insert(tableName: String): IntoQueryChain {
        query += "$INSERT $tableName"
        return IntoQueryChainImp(query)
    }

    override fun delete(tableName: String): WhereQueryChain {
        query += "$DELETE $tableName"
        return WhereQueryChainImp(query)
    }

    override fun update(tableName: String): SetQueryChain {
        query += "$UPDATE $tableName"
        return SetQueryChainImp(query)
    }

    override fun createTable(tableName: String, ifNotExists: IF_NOT_EXISTS?): InitColumnsQueryChain {
        query += CREATE_TABLE
        ifNotExists?.let {
            query += " $IF_NOT_EXISTS"
        }
        query += " $tableName"

        return InitColumnsQueryChainImp(query)
    }
}