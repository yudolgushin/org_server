package com.erhn.ftknft.querybuilder.core

interface FromQueryChain : QueryChain, LimitQueryChain {
    fun from(tableName: String): WhereQueryChain
}