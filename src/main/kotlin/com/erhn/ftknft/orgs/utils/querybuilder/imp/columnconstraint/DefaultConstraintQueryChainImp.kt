package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.ColumnQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.DefaultConstraintQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.ForeignKeyQueryChain

class DefaultConstraintQueryChainImp(
    query: String,
    private val foreignKeyQueryChain: ForeignKeyQueryChain = ForeignKeyQueryChainImp(query)
) : BaseEndInitsQueryChain(query), DefaultConstraintQueryChain {

    override fun default(value: String): ColumnQueryChain {
        query += " $DEFAULT $value"
        return ColumnQueryChainImp(query)
    }

    override fun foreignKey(columnName: String): ReferenceQueryChain {
        return foreignKeyQueryChain.foreignKey(columnName)
    }
}