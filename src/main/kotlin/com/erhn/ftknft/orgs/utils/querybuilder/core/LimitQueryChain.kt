package com.erhn.ftknft.querybuilder.core

import com.erhn.ftknft.querybuilder.imp.BaseQueryChain

interface LimitQueryChain : QueryChain {

    fun limit(limit: Int): BaseQueryChain
}