package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain
import com.erhn.ftknft.querybuilder.core.ReferenceQueryChain

interface ForeignKeyQueryChain : QueryChain, EndInitsColumnsQueryChain {

    fun foreignKey(columnName: String): ReferenceQueryChain
}