package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface UniqueAndNewConstraintQueryChain : QueryChain, UniqueConstraintQueryChain, ColumnQueryChain,
    EndInitsColumnsQueryChain {

}
