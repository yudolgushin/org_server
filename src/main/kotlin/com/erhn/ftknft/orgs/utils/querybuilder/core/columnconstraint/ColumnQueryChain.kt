package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface ColumnQueryChain : QueryChain, EndInitsColumnsQueryChain {

    fun column(columnName: String, type: Type): AllConstraintQueryChain
}