package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.*

class AllConstraintWithoutPrimaryQueryChainImp(
    query: String,
    private val column: ColumnQueryChain = ColumnQueryChainImp(query),
    private val default: DefaultConstraintQueryChain = DefaultConstraintQueryChainImp(query),
    private val unique: UniqueConstraintQueryChain = UniqueConstraintQueryChainImp(query),
    private val notNull: NotNullConstrainQueryChain = NotNullConstraintQueryChainImp(query),
    val autoincrementQueryChain: AutoincrementQueryChain = AutoincrementQueryChainImp(query),
    private val foreignKeyQueryChain: ForeignKeyQueryChain = ForeignKeyQueryChainImp(query)
) : BaseEndInitsQueryChain(query), AllConstraintWithoutPrimaryQueryChain {

    override fun column(columnName: String, type: Type): AllConstraintQueryChain {
        return column.column(columnName, type)
    }

    override fun default(value: String): ColumnQueryChain {
        return default.default(value)
    }

    override fun unique(): ColumnQueryChain {
        return unique.unique()
    }

    override fun notNull(): UniqueAndNewConstraintQueryChain {
        return notNull.notNull()
    }

    override fun autoincrement(): NotNullConstrainQueryChain {
        return autoincrementQueryChain.autoincrement()
    }

    override fun foreignKey(columnName: String): ReferenceQueryChain {
        return foreignKeyQueryChain.foreignKey(columnName)
    }
}