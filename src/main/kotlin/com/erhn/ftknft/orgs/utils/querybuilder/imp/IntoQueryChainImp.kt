package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.IntoQueryChain
import com.erhn.ftknft.querybuilder.core.ValuesQueryChain

class IntoQueryChainImp(query: String) : BaseQueryChain(query), IntoQueryChain {

    override fun into(vararg columnNames: String): ValuesQueryChain {
        if (columnNames.size == 0) return ValuesQueryChainImp(query)
        columnNames.forEachIndexed { index, s ->
            if (index == 0) {
                query += " ($s"
            } else {
                query += ", $s"
            }

        }
        query += ")"
        return ValuesQueryChainImp(query)
    }
}