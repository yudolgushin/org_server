package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.LimitQueryChain

class LimitQueryChainImp(query: String) : BaseQueryChain(query), LimitQueryChain {

    override fun limit(limit: Int): BaseQueryChain {
        query += " $LIMIT $limit"
        return LimitQueryChainImp(query)
    }
}