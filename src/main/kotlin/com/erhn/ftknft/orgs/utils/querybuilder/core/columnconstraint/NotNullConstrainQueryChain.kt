package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface NotNullConstrainQueryChain : QueryChain, EndInitsColumnsQueryChain, ForeignKeyQueryChain {

    fun notNull(): UniqueAndNewConstraintQueryChain

}