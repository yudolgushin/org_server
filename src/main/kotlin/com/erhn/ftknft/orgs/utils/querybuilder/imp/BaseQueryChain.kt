package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.QueryChain

abstract class BaseQueryChain(protected var query: String) : QueryChain {

    override fun build(): String {
        query = query + ";"
        return query
    }

    companion object {
        const val STAR = "*"
        const val SELECT = "SELECT"
        const val FROM = "FROM"
        const val WHERE = "WHERE"
        const val AND = "AND"
        const val OR = "OR"
        const val ORDER_BY = "ORDER BY"
        const val DESC = "DESC"
        const val LIMIT = "LIMIT"
        const val INSERT = "INSERT INTO"
        const val VALUES = "VALUES"
        const val DELETE = "DELETE FROM"
        const val UPDATE = "UPDATE"
        const val SET = "SET"
        const val CREATE_TABLE = "CREATE TABLE"
        const val IF_NOT_EXISTS = "IF NOT EXISTS"
        const val UNIQUE = "UNIQUE"
        const val NOT_NULL = "NOT NULL"
        const val DEFAULT = "DEFAULT"
        const val AUTOINCREMENT = "AUTOINCREMENT"
        const val PRIMARY_KEY = "PRIMARY KEY"
        const val FOREIGN_KEY = "FOREIGN KEY"
        const val REFERENCES = "REFERENCES"
    }


}