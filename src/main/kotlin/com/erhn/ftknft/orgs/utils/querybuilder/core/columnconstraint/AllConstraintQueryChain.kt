package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface AllConstraintQueryChain : QueryChain, PrimaryKeyQueryChain, NotNullConstrainQueryChain, ForeignKeyQueryChain,
    DefaultConstraintQueryChain, ColumnQueryChain, EndInitsColumnsQueryChain, UniqueConstraintQueryChain {


}