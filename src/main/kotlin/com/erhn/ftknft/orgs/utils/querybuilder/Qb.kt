package com.erhn.ftknft.querybuilder

import com.erhn.ftknft.querybuilder.core.InitialQueryChain
import com.erhn.ftknft.querybuilder.imp.InitialQueryChainImp

object Qb {
    fun init(): InitialQueryChain = InitialQueryChainImp("")
}