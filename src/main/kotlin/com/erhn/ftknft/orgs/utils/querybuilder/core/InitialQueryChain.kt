package com.erhn.ftknft.querybuilder.core

import com.erhn.ftknft.querybuilder.core.columnconstraint.InitColumnsQueryChain


interface InitialQueryChain : QueryChain {
    fun select(vararg columns: String): FromQueryChain

    fun insert(tableName: String): IntoQueryChain

    fun delete(tableName: String): WhereQueryChain

    fun update(tableName: String): SetQueryChain

    fun createTable(tableName: String, ifNotExists: IF_NOT_EXISTS? = null): InitColumnsQueryChain
}