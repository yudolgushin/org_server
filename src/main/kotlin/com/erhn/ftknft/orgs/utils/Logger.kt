package com.erhn.ftknft.orgs.utils

object Logger {
    private var queryLog = false
    private var executeLog = false

    fun executeLog(flag: Boolean): Logger {
        executeLog = flag
        return this
    }

    fun queryLog(flag: Boolean): Logger {
        queryLog = flag
        return this
    }

    fun enableQueryLog(block: () -> Unit) {
        queryLog.isTrue {
            block()
        }
    }

    fun enableExecuteLog(block: () -> Unit) {
        executeLog.isTrue {
            block()
        }
    }

}



