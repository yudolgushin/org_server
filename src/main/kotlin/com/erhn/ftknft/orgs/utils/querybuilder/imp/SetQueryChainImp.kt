package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.CD
import com.erhn.ftknft.querybuilder.core.SetAndWhereQueryChain
import com.erhn.ftknft.querybuilder.core.SetQueryChain

class SetQueryChainImp(query: String) : BaseQueryChain(query), SetQueryChain {

    override fun set(columnName: String, value: String): SetAndWhereQueryChain {
        if (query.contains(SET)) {
            query += ", $columnName ${CD.EQ} $value"
        } else {
            query += " $SET $columnName ${CD.EQ} $value"
        }
        return SetAndWhereQueryChainImp(query)
    }
}