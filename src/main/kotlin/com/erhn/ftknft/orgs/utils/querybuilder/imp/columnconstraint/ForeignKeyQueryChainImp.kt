package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.ForeignKeyQueryChain

class ForeignKeyQueryChainImp(query: String) : BaseEndInitsQueryChain(query), ForeignKeyQueryChain {

    override fun foreignKey(columnName: String): ReferenceQueryChain {
        query += ", $FOREIGN_KEY ($columnName)"
        return ReferenceQueryChainImp(query)
    }
}