package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.columnconstraint.AutoincrementQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.NotNullConstrainQueryChain

class AutoincrementQueryChainImp(query: String) : BaseEndInitsQueryChain(query), AutoincrementQueryChain {

    override fun autoincrement(): NotNullConstrainQueryChain {
        query += " $AUTOINCREMENT"
        return NotNullConstraintQueryChainImp(query)
    }
}