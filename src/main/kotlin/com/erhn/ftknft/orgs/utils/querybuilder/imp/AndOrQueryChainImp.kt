package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.*

class AndOrQueryChainImp(
    query: String, val orderBy: OrderByQueryChain = OrderByQueryChainImp(query),
    val limitQueryChain: LimitQueryChain = LimitQueryChainImp(query)
) :
    BaseQueryChain(query), AndOrQueryChain {

    override fun and(columnName: String, condition: CD, value: String): LimitQueryChain {
        query += " $AND $columnName ${condition} $value"
        return AndOrQueryChainImp(query)
    }

    override fun or(columnName: String, condition: CD, value: String): LimitQueryChain {
        query += " $OR $columnName ${condition} $value"
        return AndOrQueryChainImp(query)
    }

    override fun orderBy(columnName: String, desc: DESC?): LimitQueryChain {
        return orderBy.orderBy(columnName, desc)
    }

    override fun limit(limit: Int): BaseQueryChain {
        return limitQueryChain.limit(limit)
    }
}
