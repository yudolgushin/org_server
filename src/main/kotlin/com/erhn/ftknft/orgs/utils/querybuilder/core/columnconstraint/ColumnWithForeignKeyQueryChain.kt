package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface ColumnWithForeignKeyQueryChain : QueryChain, ColumnQueryChain, ForeignKeyQueryChain {
}