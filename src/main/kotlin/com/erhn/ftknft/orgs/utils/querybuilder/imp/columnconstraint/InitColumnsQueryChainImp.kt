package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.columnconstraint.ColumnQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.InitColumnsQueryChain

class InitColumnsQueryChainImp(query: String) : BaseEndInitsQueryChain(query), InitColumnsQueryChain {
    override fun iniColumns(): ColumnQueryChain {
        query += " ("
        return ColumnQueryChainImp(query, true)
    }
}