package com.erhn.ftknft.orgs.utils

import com.google.gson.Gson

fun <T : Verifiable> Gson.fromJsonVerify(json: String, classOfT: Class<T>): T {

        val b = fromJson(json, classOfT)
        b.verify()
        return b

}