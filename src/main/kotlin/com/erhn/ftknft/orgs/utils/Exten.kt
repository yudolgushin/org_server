package com.erhn.ftknft.orgs.utils

import com.google.gson.Gson
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.text.SimpleDateFormat
import java.util.*

fun date() = SimpleDateFormat("dd/MM/yyyy, HH:mm:ss").format(Date())

fun Any.log(msg: String?) {
    println("${date()}, ${javaClass.simpleName}: $msg")
}

fun timeLog(msg: String?) {
    println("${date()}, $msg")
}

fun Any.logln(msg: String?) {
    println()
    log(msg)
    println()
}

fun Connection.logPrepareStatement(query: String): PreparedStatement {
    Logger.enableQueryLog {
        timeLog(query)
    }
    return this.prepareStatement(query)
}

fun PreparedStatement.logExecute(): Boolean {
    val startExecute = System.currentTimeMillis()
    val execute = this.execute()
    Logger.enableExecuteLog {
        timeLog("execute time: ${System.currentTimeMillis() - startExecute} ms")
    }
    return execute
}

fun PreparedStatement.setLongLog(pos: Int, value: Long) {
    Logger.enableExecuteLog {
        timeLog("$pos: $value")
    }
    setLong(pos, value)
}

fun PreparedStatement.setDoubleLog(pos: Int, value: Double) {
    Logger.enableExecuteLog {
        timeLog("$pos: $value")
    }
    setDouble(pos, value)
}

fun PreparedStatement.setStringLog(pos: Int, value: String?) {
    Logger.enableExecuteLog {
        timeLog("$pos: $value")
    }
    setString(pos, value)
}

fun PreparedStatement.setBooleanLog(pos: Int, value: Boolean) {
    Logger.enableExecuteLog {
        timeLog("$pos: $value")
    }
    setBoolean(pos, value)
}

fun PreparedStatement.setObjectLog(pos: Int, value: Any) {
    Logger.enableExecuteLog {
        timeLog("$pos: ${Gson().toJson(value)}")
    }
    setObject(pos, value)
}


fun PreparedStatement.logExecuteQuery(): ResultSet {
    val startExecute = System.currentTimeMillis()
    val execute = this.executeQuery()
    Logger.enableExecuteLog {
        timeLog("execute query time: ${System.currentTimeMillis() - startExecute} ms")
    }
    return execute
}

inline fun Boolean.isTrue(block: (bool: Boolean) -> Unit): Boolean {
    if (this == true) {
        block(this)
    }
    return this
}

inline fun Boolean.isFalse(block: (bool: Boolean) -> Unit): Boolean {
    if (this != true) {
        block(this)
    }
    return this
}

fun String.toLongOrZero(): Long {
    return toLongOrNull() ?: 0L
}

fun String.toLongOrCurrentDateMs(): Long {
    return toLongOrNull() ?: System.currentTimeMillis()
}

fun Long.toDateFormat(pattern: String = "dd/MM/yyyy, HH:mm:ss"): String {
    val sdf = SimpleDateFormat(pattern)
    return sdf.format(Date(this))
}