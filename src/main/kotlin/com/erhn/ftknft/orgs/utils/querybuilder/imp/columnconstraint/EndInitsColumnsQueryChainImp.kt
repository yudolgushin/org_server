package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.columnconstraint.EndInitsColumnsQueryChain
import com.erhn.ftknft.querybuilder.imp.BaseQueryChain

class EndInitsColumnsQueryChainImp(query: String) : BaseEndInitsQueryChain(query), EndInitsColumnsQueryChain {

    override fun endInits(): BaseQueryChain {
        query += ")"
        return this
    }
}