package com.erhn.ftknft.orgs.utils

class Ref<T> {
    private var v: T? = null

    fun setValue(value: T?) {
        v = value
    }


    fun getValue() = v
}