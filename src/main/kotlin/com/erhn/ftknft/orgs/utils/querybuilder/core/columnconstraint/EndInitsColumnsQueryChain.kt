package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain
import com.erhn.ftknft.querybuilder.imp.BaseQueryChain

interface EndInitsColumnsQueryChain : QueryChain {

    fun endInits(): BaseQueryChain
}