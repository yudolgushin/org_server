package com.erhn.ftknft.querybuilder.core

interface QueryChain {

    fun build(): String
}