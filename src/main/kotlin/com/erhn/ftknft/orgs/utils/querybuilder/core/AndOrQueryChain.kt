package com.erhn.ftknft.querybuilder.core

interface AndOrQueryChain : QueryChain, OrderByQueryChain, LimitQueryChain {
    fun and(columnName: String, condition: CD, value: String): LimitQueryChain

    fun or(columnName: String, condition: CD, value: String): LimitQueryChain
}
