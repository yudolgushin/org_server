package com.erhn.ftknft.querybuilder.core

interface ValuesQueryChain : QueryChain {
    fun values(vararg values: String): ValuesQueryChain

}
