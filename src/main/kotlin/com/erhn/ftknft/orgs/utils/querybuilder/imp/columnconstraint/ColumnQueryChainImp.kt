package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.columnconstraint.AllConstraintQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.ColumnQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.Type

class ColumnQueryChainImp(query: String, private val isFirst: Boolean = false) : BaseEndInitsQueryChain(query),
    ColumnQueryChain {

    override fun column(columnName: String, type: Type): AllConstraintQueryChain {
        if (!isFirst) {
            query += ", $columnName $type"
        } else {
            query += "$columnName $type"
        }
        return AllConstraintQueryChainImp(query)
    }
}