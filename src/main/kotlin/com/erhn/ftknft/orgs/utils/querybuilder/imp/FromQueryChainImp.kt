package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.FromQueryChain
import com.erhn.ftknft.querybuilder.core.LimitQueryChain

class FromQueryChainImp(query: String, val limitQueryChain: LimitQueryChain = LimitQueryChainImp(query)) :
    BaseQueryChain(query), FromQueryChain {
    override fun from(tableName: String): WhereQueryChainImp {
        query += " $FROM $tableName"
        return WhereQueryChainImp(query)
    }


    override fun limit(limit: Int): BaseQueryChain {
        return limitQueryChain.limit(limit)
    }
}