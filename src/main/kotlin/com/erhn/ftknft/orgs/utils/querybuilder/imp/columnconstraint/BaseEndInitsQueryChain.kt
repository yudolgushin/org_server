package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.columnconstraint.EndInitsColumnsQueryChain
import com.erhn.ftknft.querybuilder.imp.BaseQueryChain

abstract class BaseEndInitsQueryChain(query: String) : BaseQueryChain(query), EndInitsColumnsQueryChain {

    override fun endInits(): BaseQueryChain {
        query += ")"
        return this
    }
}