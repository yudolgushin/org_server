package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.*

class SetAndWhereQueryChainImp(
    query: String,
    val setQueryChain: SetQueryChain = SetQueryChainImp(query),
    val whereQueryChain: WhereQueryChain = WhereQueryChainImp(query)
) : BaseQueryChain(query), SetAndWhereQueryChain {
    override fun set(columnName: String, value: String): SetAndWhereQueryChain {
        return setQueryChain.set(columnName, value)
    }

    override fun where(columnName: String, condition: CD, value: String): AndOrQueryChain {
        return whereQueryChain.where(columnName, condition, value)
    }

    override fun orderBy(columnName: String, desc: DESC?): LimitQueryChain {
        return whereQueryChain.orderBy(columnName, desc)
    }

    override fun limit(limit: Int): BaseQueryChain {
        return whereQueryChain.limit(limit)
    }
}