package com.erhn.ftknft.orgs.utils.config

import com.google.gson.annotations.SerializedName

data class ConfigModel(
    @SerializedName("secret_key")
    val secretKey: String
)