package com.erhn.ftknft.querybuilder.core

interface SetAndWhereQueryChain : QueryChain, SetQueryChain, WhereQueryChain {
}