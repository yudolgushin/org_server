package com.erhn.ftknft.querybuilder.core


interface WhereQueryChain : QueryChain, OrderByQueryChain, LimitQueryChain {
    fun where(columnName: String, condition: CD, value: String): AndOrQueryChain
}