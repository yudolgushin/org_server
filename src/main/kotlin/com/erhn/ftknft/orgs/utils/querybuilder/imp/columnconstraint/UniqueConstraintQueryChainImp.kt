package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.ColumnQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.ForeignKeyQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.UniqueConstraintQueryChain

class UniqueConstraintQueryChainImp(
    query: String,
    private val foreignKeyQueryChain: ForeignKeyQueryChain = ForeignKeyQueryChainImp(
        query
    )
) : BaseEndInitsQueryChain(query), UniqueConstraintQueryChain {

    override fun unique(): ColumnQueryChain {
        query += " $UNIQUE"
        return ColumnQueryChainImp(query)
    }

    override fun foreignKey(columnName: String): ReferenceQueryChain {
        return foreignKeyQueryChain.foreignKey(columnName)
    }
}