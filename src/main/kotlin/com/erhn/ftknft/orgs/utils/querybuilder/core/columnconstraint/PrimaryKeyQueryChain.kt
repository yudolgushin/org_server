package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface PrimaryKeyQueryChain : QueryChain, EndInitsColumnsQueryChain {
    fun primaryKey(): AllConstraintWithoutPrimaryQueryChain
}