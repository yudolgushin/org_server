package com.erhn.ftknft.querybuilder.core

interface OrderByQueryChain : QueryChain {
    fun orderBy(columnName: String, desc: DESC? = null): LimitQueryChain
}