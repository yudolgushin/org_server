package com.erhn.ftknft.orgs.utils

import java.util.*

object TokenUtil {
    fun generate(): String = UUID.randomUUID().toString() + "-" + UUID.randomUUID().toString()
}