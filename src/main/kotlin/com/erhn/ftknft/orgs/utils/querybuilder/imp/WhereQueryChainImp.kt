package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.*

class WhereQueryChainImp(
    query: String, val orderBy: OrderByQueryChain = OrderByQueryChainImp(query),
    val limitQueryChain: LimitQueryChain = LimitQueryChainImp(query)
) :
    BaseQueryChain(query), WhereQueryChain {


    override fun where(columnName: String, condition: CD, value: String): AndOrQueryChain {
        query += " $WHERE $columnName ${condition} $value"
        return AndOrQueryChainImp(query)
    }

    override fun orderBy(columnName: String, desc: DESC?): LimitQueryChain {
        return orderBy.orderBy(columnName, desc)
    }

    override fun limit(limit: Int): BaseQueryChain {
        return limitQueryChain.limit(limit)
    }
}