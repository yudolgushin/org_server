package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.*

class AllConstraintQueryChainImp(
    query: String,
    private val primaryKey: PrimaryKeyQueryChain = PrimaryKeyQueryChainImp(query),
    private val notNull: NotNullConstrainQueryChain = NotNullConstraintQueryChainImp(query),
    private val default: DefaultConstraintQueryChain = DefaultConstraintQueryChainImp(query),
    private val column: ColumnQueryChain = ColumnQueryChainImp(query),
    private val unique: UniqueConstraintQueryChain = UniqueConstraintQueryChainImp(query),
    private val foreignKey: ForeignKeyQueryChain = ForeignKeyQueryChainImp(query)
) : BaseEndInitsQueryChain(query), AllConstraintQueryChain {

    override fun primaryKey(): AllConstraintWithoutPrimaryQueryChain {
        return primaryKey.primaryKey()
    }

    override fun notNull(): UniqueAndNewConstraintQueryChain {
        return notNull.notNull()
    }

    override fun default(value: String): ColumnQueryChain {
        return default.default(value)
    }

    override fun column(columnName: String, type: Type): AllConstraintQueryChain {
        return column.column(columnName, type)
    }

    override fun unique(): ColumnQueryChain {
        return unique.unique()
    }

    override fun foreignKey(columnName: String): ReferenceQueryChain {
        return foreignKey.foreignKey(columnName)
    }
}