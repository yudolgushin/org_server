package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface DefaultConstraintQueryChain : QueryChain, EndInitsColumnsQueryChain, ForeignKeyQueryChain {

    fun default(value: String): ColumnQueryChain
}