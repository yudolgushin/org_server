package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.ValuesQueryChain

class ValuesQueryChainImp(query: String) : BaseQueryChain(query), ValuesQueryChain {

    override fun values(vararg values: String): ValuesQueryChain {
        if (values.size == 0) return this
        if (!query.contains(VALUES)) {
            query += " $VALUES"
        } else {
            query += ","
        }
        query += " ("
        values.forEachIndexed { index, s ->
            if (index == 0) {
                query += s
            } else {
                query += ", $s"
            }
        }
        query += ")"

        return ValuesQueryChainImp(query)
    }
}