package com.erhn.ftknft.querybuilder.core

interface IntoQueryChain : QueryChain {
    fun into(vararg columnNames: String):ValuesQueryChain
}
