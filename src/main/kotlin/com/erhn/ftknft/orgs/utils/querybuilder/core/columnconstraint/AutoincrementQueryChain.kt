package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface AutoincrementQueryChain : QueryChain,EndInitsColumnsQueryChain {

    fun autoincrement(): NotNullConstrainQueryChain
}