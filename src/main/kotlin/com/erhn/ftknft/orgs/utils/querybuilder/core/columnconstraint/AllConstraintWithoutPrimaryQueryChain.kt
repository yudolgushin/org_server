package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface AllConstraintWithoutPrimaryQueryChain : QueryChain, ColumnQueryChain, DefaultConstraintQueryChain,
    AutoincrementQueryChain,
    UniqueConstraintQueryChain, NotNullConstrainQueryChain, EndInitsColumnsQueryChain {
}