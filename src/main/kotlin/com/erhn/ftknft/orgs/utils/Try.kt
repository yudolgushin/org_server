package com.erhn.ftknft.orgs.utils

sealed class Try<out A> {



    class Success<out A>(val result: A) : Try<A>()
    class Error(val error: Throwable) : Try<Nothing>()

    companion object {
        fun <A> invoke(block: () -> A): Try<A> {
            return try {
                Success(block())
            } catch (e: Throwable) {
                Error(e)
            }
        }

    }

    fun <B> map(block: (A) -> B): Try<B> = flatMap { Success(block(it)) }

    fun <B> flatMap(block: (it: A) -> Try<B>): Try<B> {
        return when (this) {
            is Success -> block(result)
            is Error -> this
        }
    }
}