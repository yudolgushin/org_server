package com.erhn.ftknft.querybuilder.core

interface SetQueryChain : QueryChain {

    fun set(columnName: String, value: String): SetAndWhereQueryChain
}