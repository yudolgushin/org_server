package com.erhn.ftknft.querybuilder.core.columnconstraint

interface UniqueConstraintQueryChain : EndInitsColumnsQueryChain, ForeignKeyQueryChain {
    fun unique(): ColumnQueryChain
}