package com.erhn.ftknft.querybuilder.core

import com.erhn.ftknft.querybuilder.core.columnconstraint.ForeignKeyQueryChain

interface ReferenceQueryChain : QueryChain {

    fun references(tableReference: String, columnReference: String): ForeignKeyQueryChain

}
