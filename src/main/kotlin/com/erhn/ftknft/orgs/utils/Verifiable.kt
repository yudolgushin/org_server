package com.erhn.ftknft.orgs.utils


interface Verifiable {

    fun verify()

    fun error(a: Any):Nothing = throw VerifiableException(a)

    private class VerifiableException(a: Any?) : RuntimeException("Can't verify ${a?.javaClass?.name}")

}
