package com.erhn.ftknft.orgs.utils.config

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import org.koin.core.KoinComponent
import java.io.File

class Config(val gson: Gson) : KoinComponent {
    @SerializedName("secret_key")
    private val secretKey: String

    init {
        secretKey = gson.fromJson<ConfigModel>(File("config.json").readText(), ConfigModel::class.java).secretKey
    }

    fun secretKey() = secretKey
}