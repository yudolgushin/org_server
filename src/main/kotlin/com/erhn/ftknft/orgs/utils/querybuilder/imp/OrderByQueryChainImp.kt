package com.erhn.ftknft.querybuilder.imp

import com.erhn.ftknft.querybuilder.core.DESC
import com.erhn.ftknft.querybuilder.core.LimitQueryChain
import com.erhn.ftknft.querybuilder.core.OrderByQueryChain

class OrderByQueryChainImp(query: String) : BaseQueryChain(query), OrderByQueryChain {

    override fun orderBy(columnName: String, desc: DESC?): LimitQueryChain {
        query += " $ORDER_BY $columnName${desc?.let { " " + DESC } ?: ""}"
        return LimitQueryChainImp(query)
    }
}