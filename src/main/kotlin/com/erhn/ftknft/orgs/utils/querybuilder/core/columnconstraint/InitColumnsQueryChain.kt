package com.erhn.ftknft.querybuilder.core.columnconstraint

import com.erhn.ftknft.querybuilder.core.QueryChain

interface InitColumnsQueryChain : QueryChain, EndInitsColumnsQueryChain {
    fun iniColumns(): ColumnQueryChain
}