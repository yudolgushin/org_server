package com.erhn.ftknft.querybuilder.imp.columnconstraint

import com.erhn.ftknft.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.querybuilder.core.columnconstraint.ForeignKeyQueryChain

class ReferenceQueryChainImp(query: String) : BaseEndInitsQueryChain(query), ReferenceQueryChain {
    override fun references(tableReference: String, columnReference: String): ForeignKeyQueryChain {
        query += " $REFERENCES $tableReference($columnReference)"
        return ForeignKeyQueryChainImp(query)
    }
}