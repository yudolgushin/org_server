package com.erhn.ftknft.orgs.data.model

data class Token(
    val id: Long,
    val userId: Long,
    val token: String,
    val expirationTime: Long
)