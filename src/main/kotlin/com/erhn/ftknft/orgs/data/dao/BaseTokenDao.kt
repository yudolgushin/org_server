package com.erhn.ftknft.orgs.data.dao

import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.utils.logExecute
import com.erhn.ftknft.orgs.utils.logPrepareStatement
import com.erhn.ftknft.orgs.utils.setLongLog
import com.erhn.ftknft.orgs.utils.setStringLog
import java.sql.Connection
import java.sql.ResultSet

abstract class BaseTokenDao(conn: Connection) : BaseDao<Token>(conn) {


    override fun toDaoObject(rs: ResultSet): Token {
        return Token(rs.getLong(1), rs.getLong(2), rs.getString(3), rs.getLong(4))
    }


    open fun insert(token: String, userId: Long) {
        val p =
            conn.logPrepareStatement("INSERT INTO ${tableName()} (${DbConst.TOKEN}, ${DbConst.EXPIRATION_TIME}, ${DbConst.USER_ID}) VALUES (?,?,?)")
        p.setStringLog(1, token)
        p.setLongLog(2, expirationTime() + System.currentTimeMillis())
        p.setLongLog(3, userId)
        p.logExecute()
    }

    open fun removeExpired() {
        val p = conn.logPrepareStatement("DELETE FROM ${tableName()} WHERE ${DbConst.EXPIRATION_TIME} < ?")
        p.setLongLog(1, System.currentTimeMillis())
        p.logExecute()
    }


    abstract fun expirationTime(): Long

}