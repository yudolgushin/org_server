package com.erhn.ftknft.orgs.data.dao.imp.notes

import com.erhn.ftknft.orgs.data.dao.BaseDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Note
import com.erhn.ftknft.orgs.utils.logExecute
import com.erhn.ftknft.orgs.utils.logPrepareStatement
import com.erhn.ftknft.orgs.utils.setLongLog
import com.erhn.ftknft.orgs.utils.setStringLog
import java.sql.Connection
import java.sql.ResultSet

class NoteDao(conn: Connection) : BaseDao<Note>(conn) {

    override fun tableName(): String = DbConst.NOTES_TABLE

    override fun toDaoObject(rs: ResultSet): Note {
        return Note(
            rs.getLong(1), rs.getLong(2), rs.getString(3), rs.getString(4), rs.getLong(5), rs.getLong(6)
        )
    }


    fun insert(userId: Long, title: String, text: String) {
        val p = conn.logPrepareStatement(
            "INSERT INTO ${tableName()} " + "(${DbConst.USER_ID}, " + "${DbConst.CREATE_DATE}, " + "${DbConst.UPDATE_DATE}," + "${DbConst.NOTE_TITLE}," + "${DbConst.NOTE_TEXT}) " + "VALUES (?,?,?,?,?)"
        )

        p.setLongLog(1, userId)
        p.setLongLog(2, System.currentTimeMillis())
        p.setLongLog(3, System.currentTimeMillis())
        p.setStringLog(4, title)
        p.setStringLog(5, text)
        p.logExecute()
    }


    fun update(id: Long, title: String, text: String) {
        val p =
            conn.logPrepareStatement("UPDATE ${tableName()} SET ${DbConst.NOTE_TITLE} = ?, ${DbConst.NOTE_TEXT} = ?, ${DbConst.UPDATE_DATE} = ? WHERE ${DbConst.ID} = ?")

        p.setStringLog(1, title)
        p.setStringLog(2, text)
        p.setLongLog(3, System.currentTimeMillis())
        p.setLongLog(4, id)
        p.logExecute()
    }
}