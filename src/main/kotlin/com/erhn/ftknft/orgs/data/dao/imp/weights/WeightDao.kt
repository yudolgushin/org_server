package com.erhn.ftknft.orgs.data.dao.imp.weights

import com.erhn.ftknft.orgs.data.dao.BaseDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Weight
import com.erhn.ftknft.orgs.utils.*
import java.sql.Connection
import java.sql.ResultSet

class WeightDao(conn: Connection) : BaseDao<Weight>(conn) {
    override fun tableName(): String = DbConst.WEIGHTS_TABLE

    override fun toDaoObject(rs: ResultSet): Weight {
        return rs.run {
            Weight(getLong(1), getLong(2), getDouble(3), getLong(4))
        }
    }


    fun insertNew(userId: Long, weightValue: Double, weightDate: Long) {
        val p =
            conn.logPrepareStatement("INSERT INTO ${tableName()} (${DbConst.USER_ID}, ${DbConst.WEIGHT_VALUE}, ${DbConst.WEIGHT_DATE} ) VALUES (?, ?, ?)")
        p.apply {
            setLongLog(1, userId)
            setDoubleLog(2, weightValue)
            setLongLog(3, weightDate)
        }
        p.logExecute()
    }

    fun insertAll(userId: Long, weights: List<Pair<Double, Long>>) {
        if (weights.isEmpty()) return
        var query = "INSERT INTO ${tableName()} (${DbConst.USER_ID}, ${DbConst.WEIGHT_VALUE}, ${DbConst.WEIGHT_DATE} ) VALUES "
        for (weight in weights) {
            query += "(?, ?, ?)"
            if (weights.indexOf(weight) == weights.lastIndex) {
                query += ";"
            } else {
                query += ","
            }
        }
        val perp = conn.logPrepareStatement(query)
        var counter = 1
        for (i in 0 until weights.size) {
            val current = weights[i]
            val j = i + counter
            perp.setLongLog(j, userId)
            perp.setDoubleLog(j + 1, current.first)
            perp.setLongLog(j + 2, current.second)
            counter += 2
        }
        perp.logExecute()
    }

    fun update(weightValue: Double, weightDate: Long, id: Long) {
        val p =
            conn.logPrepareStatement("UPDATE ${tableName()} SET ${DbConst.WEIGHT_VALUE} = ?," + " ${DbConst.WEIGHT_DATE} = ? WHERE ${DbConst.ID} = ?")
        p.setDoubleLog(1, weightValue)
        p.setLongLog(2, weightDate)
        p.setLongLog(3, id)
        p.logExecute()

    }


    fun findWithFilters(userId: Long, startDate: Long, endDate: Long, page: Long? = null, limit: Long? = null): ArrayList<Weight> {
        val list: ArrayList<Weight> = arrayListOf()
        var query =
            "SELECT * FROM ${tableName()} WHERE ${DbConst.USER_ID} = ? and ${DbConst.WEIGHT_DATE} > ? and ${DbConst.WEIGHT_DATE} < ? " +
                    "ORDER BY ${DbConst.WEIGHT_DATE} DESC"
        val isPaging = page != null && limit != null
        if (isPaging) {
            query = "$query LIMIT ? OFFSET ?"
        }
        val prep =
            conn.logPrepareStatement(
                query
            )
        prep.setLongLog(1, userId)
        prep.setLongLog(2, startDate)
        prep.setLongLog(3, endDate)
        if (isPaging) {
            prep.setLongLog(4, limit!!)
            prep.setLongLog(5, page!! * limit)
        }
        val rs = prep.logExecuteQuery()
        while (rs.next()) {
            list.add(toDaoObject(rs))
        }
        return list
    }
}