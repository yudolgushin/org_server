package com.erhn.ftknft.orgs.data.model

import com.google.gson.annotations.SerializedName

data class Exercise(
    val id: Long,
    val userId: Long,
    val name: String,
    val description: String?,
    val type: Type)
{
    enum class Type(val value: String) {
        @SerializedName("by_time")
        BY_TIME("by_time"),
        @SerializedName("by_count")
        BY_COUNT("by_count");
    }

    companion object {
        fun cast(textValue:String):Type {
            for (value in Type.values()) {
                if (value.value == textValue){
                    return value
                }
            }
            throw RuntimeException("Wrong exercise type $textValue")
        }
    }
}