package com.erhn.ftknft.orgs.data.model.socket

import io.vertx.core.http.ServerWebSocket

class SocketConnection(val userId: Long, val serverWebSocket: ServerWebSocket) {
    fun write(element: String) {
        serverWebSocket.writeTextMessage(element)
    }
}