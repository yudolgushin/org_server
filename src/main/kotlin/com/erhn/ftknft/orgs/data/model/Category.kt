package com.erhn.ftknft.orgs.data.model

data class Category(
        val id: Long,
        val user_id: Long,
        val name: String,
        val type: String,
        val date: Long
)