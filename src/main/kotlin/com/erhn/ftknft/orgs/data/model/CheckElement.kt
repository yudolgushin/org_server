package com.erhn.ftknft.orgs.data.model

data class CheckElement (
    val id: Long,
    val checkListId: Long,
    val name: String,
    val isComplete: Boolean
)