package com.erhn.ftknft.orgs.data.dao.imp.weights

import com.erhn.ftknft.orgs.data.dao.BaseDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.TargetWeight
import com.erhn.ftknft.orgs.utils.logExecute
import com.erhn.ftknft.orgs.utils.logPrepareStatement
import com.erhn.ftknft.orgs.utils.setDoubleLog
import com.erhn.ftknft.orgs.utils.setLongLog
import java.sql.Connection
import java.sql.ResultSet

class TargetWeightsDao(conn: Connection) : BaseDao<TargetWeight>(conn) {

    override fun tableName(): String = DbConst.TARGET_WEIGHTS_TABLE

    override fun toDaoObject(rs: ResultSet): TargetWeight = rs.let { TargetWeight(it.getLong(1), it.getLong(2), it.getDouble(3)) }

    fun insert(userId: Long, value: Double?) {
        if (value == null) {
            removeByUserId(userId)
            return
        }
        val query = "INSERT OR REPLACE INTO ${tableName()} (${DbConst.USER_ID}, ${DbConst.TARGET_WEIGHT_VALUE}) VALUES (?, ?)"
        val perp = conn.logPrepareStatement(query)
        perp.setLongLog(1, userId)
        perp.setDoubleLog(2, value)
        perp.logExecute()

    }

    fun removeByUserId(userId: Long) {
        removeByParam(DbConst.USER_ID, userId)
    }

}