package com.erhn.ftknft.orgs.data.dao.imp.auth

import com.erhn.ftknft.orgs.data.dao.BaseTokenDao
import com.erhn.ftknft.orgs.data.db.DbConst
import java.sql.Connection
import java.util.concurrent.TimeUnit

class RefreshTokenDao(conn: Connection) : BaseTokenDao(conn) {

    override fun expirationTime(): Long = TimeUnit.DAYS.toMillis(21)

    override fun tableName(): String = DbConst.REFRESH_TOKENS_TABLE
}
