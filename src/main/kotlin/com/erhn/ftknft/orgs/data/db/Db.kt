package com.erhn.ftknft.orgs.data.db

import com.erhn.ftknft.orgs.data.dao.imp.auth.AccessTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.auth.RefreshTokenDao
import com.erhn.ftknft.orgs.data.dao.imp.auth.UserDao
import com.erhn.ftknft.orgs.data.dao.imp.notes.NoteDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.TargetWeightsDao
import com.erhn.ftknft.orgs.data.dao.imp.weights.WeightDao
import com.erhn.ftknft.orgs.utils.logExecute
import com.erhn.ftknft.orgs.utils.logPrepareStatement
import com.erhn.ftknft.querybuilder.Qb
import com.erhn.ftknft.querybuilder.core.IF_NOT_EXISTS
import com.erhn.ftknft.querybuilder.core.columnconstraint.Type
import org.sqlite.SQLiteConfig
import java.sql.Connection
import java.sql.DriverManager


class Db {

    private val conn: Connection
    val userDao: UserDao
    val accessTokenDao: AccessTokenDao
    val refreshTokenDao: RefreshTokenDao
    val weightDao: WeightDao
    val noteDao: NoteDao
    val targetWeightDao: TargetWeightsDao

    init {
        Class.forName("org.sqlite.JDBC")
        val config = SQLiteConfig()
        config.enforceForeignKeys(true)
        val name = "Db.db"
        conn = DriverManager.getConnection("jdbc:sqlite:$name", config.toProperties())
        initTables()
        userDao = UserDao(conn)
        accessTokenDao = AccessTokenDao(conn)
        refreshTokenDao = RefreshTokenDao(conn)
        weightDao = WeightDao(conn)
        noteDao = NoteDao(conn)
        targetWeightDao = TargetWeightsDao(conn)
        removeExpiredTokens()
    }

    private fun initTables() {
        //Create users table
        conn.logPrepareStatement(
            "CREATE TABLE IF NOT EXISTS ${DbConst.USERS_TABLE} (${DbConst.ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "${DbConst.LOGIN} TEXT NOT NULL UNIQUE, " +
                    "${DbConst.PASSWORD_HASH} TEXT NOT NULL, " +
                    "${DbConst.DISPLAY_NAME} TEXT);"
        ).logExecute()
        //Create tokens tables
        conn.logPrepareStatement(
            "CREATE TABLE IF NOT EXISTS ${DbConst.ACCESS_TOKENS_TABLE} (${DbConst.ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "${DbConst.USER_ID} INTEGER NOT NULL, " +
                    "${DbConst.TOKEN} TEXT NOT NULL, " +
                    "${DbConst.EXPIRATION_TIME} INTEGER NOT NULL, " +
                    "FOREIGN KEY (${DbConst.USER_ID}) REFERENCES ${DbConst.USERS_TABLE}(${DbConst.ID}) ON DELETE CASCADE);"
        ).logExecute()

        conn.logPrepareStatement(
            "CREATE TABLE IF NOT EXISTS ${DbConst.REFRESH_TOKENS_TABLE} (${DbConst.ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "${DbConst.USER_ID} INTEGER NOT NULL, " +
                    "${DbConst.TOKEN} TEXT NOT NULL, " +
                    "${DbConst.EXPIRATION_TIME} INTEGER NOT NULL, " +
                    "FOREIGN KEY (${DbConst.USER_ID}) REFERENCES ${DbConst.USERS_TABLE}(${DbConst.ID}) ON DELETE CASCADE);"
        ).logExecute()

        //Create weights table
        conn.logPrepareStatement(
            "CREATE TABLE IF NOT EXISTS ${DbConst.WEIGHTS_TABLE} (${DbConst.ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "${DbConst.USER_ID} INTEGER NOT NULL, " +
                    "${DbConst.WEIGHT_VALUE} REAL NOT NULL, " +
                    "${DbConst.WEIGHT_DATE} INTEGER NOT NULL, " +
                    "FOREIGN KEY (${DbConst.USER_ID}) REFERENCES ${DbConst.USERS_TABLE}(${DbConst.ID}) ON DELETE CASCADE);"
        ).logExecute()

        // Create categories table
        conn.logPrepareStatement(
            "CREATE TABLE IF NOT EXISTS ${DbConst.CATEGORIES_TABLE} (${DbConst.ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "${DbConst.USER_ID} INTEGER NOT NULL, " +
                    "${DbConst.CATEGORY_NAME} TEXT NOT NULL, " +
                    "${DbConst.CATEGORY_TYPE} TEXT NOT NULL, " +
                    "${DbConst.CATEGORY_DATE} INTEGER NOT NULL, " +
                    "FOREIGN KEY (${DbConst.USER_ID}) REFERENCES ${DbConst.USERS_TABLE}(${DbConst.ID}) ON DELETE CASCADE);"
        ).logExecute()

        // Create note table
        conn.logPrepareStatement(
            "CREATE TABLE IF NOT EXISTS ${DbConst.NOTES_TABLE} (${DbConst.ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "${DbConst.USER_ID} INTEGER NOT NULL, " +
                    "${DbConst.NOTE_TITLE} TEXT NOT NULL, " +
                    "${DbConst.NOTE_TEXT} TEXT NOT NULL, " +
                    "${DbConst.CREATE_DATE} INTEGER NOT NULL, " +
                    "${DbConst.UPDATE_DATE} INTEGER NOT NULL, " +
                    "FOREIGN KEY (${DbConst.USER_ID}) REFERENCES ${DbConst.USERS_TABLE}(${DbConst.ID}) ON DELETE CASCADE);"
        ).logExecute()

        //init target weights
        val qb = Qb.init().createTable(DbConst.TARGET_WEIGHTS_TABLE, IF_NOT_EXISTS)
            .iniColumns()
            .column(DbConst.ID, Type.INTEGER).primaryKey().autoincrement().notNull()
            .column(DbConst.USER_ID, Type.INTEGER).notNull().unique()
            .column(DbConst.TARGET_WEIGHT_VALUE, Type.REAL)
            .foreignKey(DbConst.USER_ID).references(DbConst.USERS_TABLE, DbConst.ID)
            .endInits()
            .build()

        conn.logPrepareStatement(qb).logExecute()
    }

    fun removeExpiredTokens() {
        accessTokenDao.removeExpired()
        refreshTokenDao.removeExpired()
    }

}