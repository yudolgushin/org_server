package com.erhn.ftknft.orgs.data.model

data class User(
    val id: Long,
    val login: String,
    val passwordHash: String,
    val displayName: String?
)