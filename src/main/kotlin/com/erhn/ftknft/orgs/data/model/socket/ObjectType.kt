package com.erhn.ftknft.orgs.data.model.socket

import com.google.gson.annotations.SerializedName

enum class ObjectType {
    @SerializedName("unknown")
    UNKNOWN,

    @SerializedName("weight")
    WEIGHT,

    @SerializedName("note")
    NOTE;


    companion object {
        fun getType(entity: Any?): ObjectType {
            return values().find { it.name.toLowerCase() == entity?.javaClass?.simpleName?.toLowerCase() } ?: UNKNOWN
        }
    }
}