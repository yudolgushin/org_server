package com.erhn.ftknft.orgs.data.dao

import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.utils.*
import com.erhn.ftknft.querybuilder.Qb
import java.sql.Connection
import java.sql.ResultSet

abstract class BaseDao<T>(protected val conn: Connection) {

    fun lastInsertedId(): Long {
        val prep = conn.logPrepareStatement("SELECT id FROM ${tableName()} ORDER BY id DESC LIMIT 1")
        val executeQuery = prep.logExecuteQuery()
        return executeQuery.getLong(1)
    }

    fun lastInserted(): T? {
        val prep = conn.logPrepareStatement("SELECT * FROM ${tableName()} ORDER BY id DESC LIMIT 1")
        val executeQuery = prep.logExecuteQuery()
        return objectFromResultSet(executeQuery)

    }

    fun getAll(): List<T> {
        val prep = conn.logPrepareStatement(Qb.init().select().from(tableName()).build())
        val executeQuery = prep.logExecuteQuery()
        val elementsList = ArrayList<T>()
        while (executeQuery.next()) {
            elementsList.add(toDaoObject(executeQuery))
        }
        return elementsList
    }

    fun findByParam(columnName: String, value: Any): ArrayList<T> {
        val list = ArrayList<T>()
        val prep =
            conn.logPrepareStatement("SELECT * FROM ${tableName()} WHERE ${columnName} = ? ORDER BY ${DbConst.ID} DESC")
        prep.setObjectLog(1, value)
        val rs = prep.logExecuteQuery()
        while (rs.next()) {
            list.add(toDaoObject(rs))
        }
        return list
    }

    fun firstFindByParam(columnName: String, value: Any): T? {
        val prep = conn.logPrepareStatement("SELECT * FROM ${tableName()} WHERE ${columnName} = ? ORDER BY id LIMIT 1")
        prep.setObjectLog(1, value)
        val rs = prep.logExecuteQuery()
        return objectFromResultSet(rs)

    }

    fun findByTwoParams(firstSpec: Pair<String, Any>, secondSpec: Pair<String, Any>): ArrayList<T> {
        val list = ArrayList<T>()
        val prep = conn.logPrepareStatement(
            "SELECT * FROM ${tableName()} WHERE ${firstSpec.first} = ? AND ${secondSpec.first} = ? ORDER BY ${DbConst.ID} DESC"
        )
        prep.setObjectLog(1, firstSpec.second)
        prep.setObjectLog(2, secondSpec.second)
        val rs = prep.logExecuteQuery()
        while (rs.next()) {
            list.add(toDaoObject(rs))
        }
        return list
    }

    fun firstFindByTwoParams(firstSpec: Pair<String, Any>, secondSpec: Pair<String, Any>): T? {
        val prep = conn.logPrepareStatement(
            "SELECT * FROM ${tableName()} WHERE ${firstSpec.first} = ? AND ${secondSpec.first} = ? ORDER BY id LIMIT 1"
        )
        prep.setObjectLog(1, firstSpec.second)
        prep.setObjectLog(2, secondSpec.second)
        val rs = prep.logExecuteQuery()
        return objectFromResultSet(rs)
    }


    fun removeById(id: Long) {
        val prep = conn.logPrepareStatement("DELETE FROM ${tableName()} WHERE ${DbConst.ID} = ?")
        prep.setLongLog(1, id)
        prep.logExecute()
    }


    fun removeByParam(columnName: String, value: Any) {
        val prep = conn.logPrepareStatement("DELETE FROM ${tableName()} WHERE ${columnName} = ?")
        prep.setObjectLog(1, value)
        prep.logExecute()
    }


    fun findById(id: Long): T? {
        val prep = conn.logPrepareStatement("SELECT * FROM ${tableName()} WHERE ${DbConst.ID} = ? ORDER BY id LIMIT 1")
        prep.setLongLog(1, id)
        val executeQuery = prep.logExecuteQuery()
        return objectFromResultSet(executeQuery)
    }

    abstract protected fun tableName(): String

    abstract protected fun toDaoObject(rs: ResultSet): T

    private fun objectFromResultSet(executeQuery: ResultSet): T? {
        if (!executeQuery.isBeforeFirst) {
            return null
        } else {
            return toDaoObject(executeQuery)
        }
    }


}