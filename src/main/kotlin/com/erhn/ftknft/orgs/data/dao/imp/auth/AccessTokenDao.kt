package com.erhn.ftknft.orgs.data.dao.imp.auth

import com.erhn.ftknft.orgs.data.dao.BaseTokenDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.Token
import com.erhn.ftknft.orgs.utils.log
import java.sql.Connection
import java.util.concurrent.TimeUnit

class AccessTokenDao(conn: Connection) : BaseTokenDao(conn) {

    val cachedAccessTokens = HashMap<String, Token>()

    override fun tableName(): String = DbConst.ACCESS_TOKENS_TABLE

    override fun expirationTime(): Long = TimeUnit.DAYS.toMillis(1)

    override fun insert(token: String, userId: Long) {
        super.insert(token, userId)
        val insertedToken = firstFindByParam(DbConst.TOKEN, token)
        insertedToken?.let {
            log("Put token to cache $token with $it")
            cachedAccessTokens.put(token, it)
        }
    }


    fun getToken(accessToken: String): Token? {
        return cachedAccessTokens.get(accessToken)?.let {
            log("Getting accessToken $accessToken from cache")
            return it
        } ?: firstFindByParam(DbConst.TOKEN, accessToken)

    }

    override fun removeExpired() {
        super.removeExpired()
        cachedAccessTokens.clear()
    }
}