package com.erhn.ftknft.orgs.data.model.socket

import com.google.gson.annotations.SerializedName

data class SocketModel(
    @SerializedName("object_type")
    val objectType: ObjectType,
    @SerializedName("event_type")
    val eventType: EventType,
    @SerializedName("date")
    val date: Long,
    @SerializedName("data")
    val data: Any
)