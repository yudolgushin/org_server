package com.erhn.ftknft.orgs.data.model

data class TargetWeight(
    val id: Long,
    val userId: Long,
    val weightValue: Double
)