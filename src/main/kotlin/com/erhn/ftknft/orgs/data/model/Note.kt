package com.erhn.ftknft.orgs.data.model

data class Note(
    val id: Long,
    val userId: Long,
    val title: String,
    val text: String,
    val createDate: Long,
    val updateDate: Long
)