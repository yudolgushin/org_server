package com.erhn.ftknft.orgs.data.model.socket

import com.google.gson.annotations.SerializedName

enum class EventType {
    @SerializedName("delete")
    DELETE,

    @SerializedName("add")
    ADD,

    @SerializedName("update")
    UPDATE

}