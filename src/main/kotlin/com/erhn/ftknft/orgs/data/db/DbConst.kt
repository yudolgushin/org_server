package com.erhn.ftknft.orgs.data.db

object DbConst {


    const val ID = "id"
    const val USER_ID = "user_id"
    const val CREATE_DATE = "create_date"
    const val UPDATE_DATE = "update_date"


    const val LOGIN = "login"
    const val PASSWORD_HASH = "password_hash"
    const val DISPLAY_NAME = "display_name"

    const val TOKEN = "token"
    const val EXPIRATION_TIME = "expiration_time"


    const val WEIGHT_VALUE = "weight_value"
    const val WEIGHT_DATE = "weight_date"

    const val CATEGORY_NAME = "name"
    const val CATEGORY_DATE = "date"
    const val CATEGORY_TYPE = "type"


    const val NOTE_TITLE = "title"
    const val NOTE_TEXT = "text"


    const val TARGET_WEIGHT_VALUE = "target_weight_value"

    const val CHECKLIST_TITLE = "title"

    const val CHECK_ELEMENT_NAME = "name"
    const val CHECKLIST_ID = "checklist_id"
    const val CHECK_ELEMENT_IS_COMPLETE = "is_complete"


    const val EXERCISE_NAME = "name"
    const val EXERCISE_TYPE = "type"
    val EXERCISE_DESCRIPTION = "description"

    //Table names
    const val USERS_TABLE = "users"
    const val ACCESS_TOKENS_TABLE = "access_tokens"
    const val REFRESH_TOKENS_TABLE = "refresh_tokens"
    const val WEIGHTS_TABLE = "weights"
    const val CATEGORIES_TABLE = "categories"
    const val NOTES_TABLE = "notes"
    const val TARGET_WEIGHTS_TABLE = "target_weights"

    const val CHECKLISTS_TABLE = "checklists"
    const val CHECK_ELEMENTS_TABLE = "check_elements"
    const val EXERCISES_TABLE_NAME = "exercises"
}