package com.erhn.ftknft.orgs.data.dao.imp.auth

import com.erhn.ftknft.orgs.data.dao.BaseDao
import com.erhn.ftknft.orgs.data.db.DbConst
import com.erhn.ftknft.orgs.data.model.User
import com.erhn.ftknft.orgs.utils.logExecute
import com.erhn.ftknft.orgs.utils.logPrepareStatement
import com.erhn.ftknft.orgs.utils.setStringLog
import java.sql.Connection
import java.sql.ResultSet

class UserDao(conn: Connection) : BaseDao<User>(conn) {


    override fun tableName(): String = DbConst.USERS_TABLE


    fun insert(login: String, passwordHash: String, displayName: String?): Long {
        val prep =
            conn.logPrepareStatement("INSERT INTO ${DbConst.USERS_TABLE} (${DbConst.LOGIN}, ${DbConst.PASSWORD_HASH}, ${DbConst.DISPLAY_NAME}) VALUES (?,?,?)")
        prep.setStringLog(1, login)
        prep.setStringLog(2, passwordHash)
        prep.setStringLog(3, displayName)
        prep.logExecute()
        return lastInsertedId()

    }


    override fun toDaoObject(rs: ResultSet): User {
        return User(
            rs.getLong(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4)
        )
    }

}