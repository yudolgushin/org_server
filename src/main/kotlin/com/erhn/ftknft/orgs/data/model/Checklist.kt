package com.erhn.ftknft.orgs.data.model

data class Checklist(
    val id: Long,
    val userId: Long,
    val title: String,
    val createDate: Long,
    val updateDate: Long
)