package com.erhn.ftknft.orgs.data.model

data class Weight(
        val id: Long,
        val userId: Long,
        val weightValue: Double,
        val dateWeight: Long
)