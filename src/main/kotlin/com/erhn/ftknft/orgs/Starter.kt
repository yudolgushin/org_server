package com.erhn.ftknft.orgs

import com.erhn.ftknft.orgs.utils.Logger
import io.vertx.core.Vertx
import org.koin.core.context.startKoin

object Starter {

    @JvmStatic
    fun main(args: Array<String>) {
        Logger.executeLog(true).queryLog(true)

        startKoin {
            modules(listOf(dataModule, handlersModule, utilsModule))
        }
        Vertx.vertx().deployVerticle(MainVerticle::class.java.name)
    }

}
