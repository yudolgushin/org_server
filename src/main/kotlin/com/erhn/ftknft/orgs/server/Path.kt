package com.erhn.ftknft.orgs.server

object Path {
    const val REGISTRATION = "registration"
    const val AUTH = "auth"
    const val REFRESH = "refresh"
    const val WEIGHTS = "weights"
    const val NOTES = "notes"
    const val ADMIN = "admin"
    const val SECRET_KEY = "secretKey"
    const val USERS = "users"
    const val CHECK_LOGIN = "check_login"
    const val USER = "user"
    const val TARGET = "target"
    const val SYNC = "sync"

}